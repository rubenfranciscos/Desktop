﻿CREATE TABLE [dbo].[Utilizador] (
    [CodUtilizador]         INT           IDENTITY (1, 1) NOT NULL,
    [TipoUtilizadorCodTipo] VARCHAR(50)           NULL,
    [Username]              VARCHAR (255) NULL,
    [Password]              VARCHAR (255) NULL,
    [Nome]                  VARCHAR (255) NULL,
    [Apelido]               VARCHAR (255) NULL,
    [Idade]                 INT           NOT NULL,
    [Morada]                VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([CodUtilizador] ASC),
    CONSTRAINT [TEM] FOREIGN KEY ([TipoUtilizadorCodTipo]) REFERENCES [dbo].[TipoUtilizador] ([CodTipo])
);

