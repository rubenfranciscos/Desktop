﻿using MySql.Data.MySqlClient;
using Park_View.Admin.Contas;
using Park_View.Admin.Inicio;
using Park_View.Admin.Parque;
using Park_View.Admin.Planta;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View
{
    public partial class Criar_Organizacao : Form
    {
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                       BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                       BDcodigo;//do alunos, caso se trate de um update ou insert
        
        private MySqlTransaction sqlTran = null;

        public Criar_Organizacao(String sgbd, String dml, String codigo)
        {
            InitializeComponent();
            BDsgbd = sgbd;
            BDdml = dml;
            BDcodigo = codigo;
        }

        private void Criar_Organizacao_Load(object sender, EventArgs e)
        {
            this.Text = BDsgbd + " " + BDdml;

            System.Windows.Forms.ToolTip btnOKToolTip = new System.Windows.Forms.ToolTip();
            btnOKToolTip.SetToolTip(this.btnOK, "ADICIONAR");

            System.Windows.Forms.ToolTip btnCancelToolTip = new System.Windows.Forms.ToolTip();
            btnCancelToolTip.SetToolTip(this.btnCncael, "VOLTAR");

            switch (BDdml)
            {
                case "Inserir":
                    tbCod.Text = getLastTablePk();
                    tbNome.Focus();
                    btnOK.BackgroundImage = Properties.Resources.add;
                    break;

                case "Update":
                    getFieldsData();
                    tbCod.Text = BDcodigo;
                    tbNome.Focus();              //Focus nesta caixa
                    btnOK.BackgroundImage = Properties.Resources.edit;
                    break;

                case "Delete":
                    getFieldsData();

                    tbCod.Text = BDcodigo;
                    tbCod.Enabled = false;
                    tbNome.Enabled = false;
                    tbDescricao.Enabled = false;
                    btnOK.Focus();

                    btnOK.BackgroundImage = Properties.Resources.delete;
                    break;

            }
        }

        private String getLastTablePk()
        {
            int pkCode = -1;                                                      // Recebe o codigo da tabela a usar nas SQL DML
            //Recebe a UtilsSQl uma ligação ao sgbd
            MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
            try
            {

                if (Utils.getTest())
                {
                    //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                }

                MySqlCommand sqlCommand = new MySqlCommand("Select MAX(CodOrganizacao) as CodOrganizacao from Organizacao", sqlConn);   //Comando SQL DML
                sqlCommand.CommandType = CommandType.Text;

                sqlConn.Open();                                             //abertura a base de dados
                MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                {
                    while (sqlDataReader.Read())                             //Enquanto houver rows
                    {
                        String temp = sqlDataReader["CodOrganizacao"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                        if (Utils.getTest()) //MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCriar Organizacao");
                        if (temp == "")
                        {
                            //MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                            pkCode = 1;                                     //registo na textbox da codigo par a a

                        }
                        else
                        {
                            if (Utils.getTest()) 
                                //MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormAluno");
                            pkCode = int.Parse(temp) + 1;
                        }
                    }
                }
                else
                {
                    //MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                    pkCode = 1;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormAluno");

            }
            finally
            {
                sqlConn.Close();
            }
            return pkCode.ToString();
        }

        private void getFieldsData()
        {
            MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
            if (Utils.getTest()) //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");
            try
            {
                MySqlCommand sqlCommand = new MySqlCommand("Select NomeOrganizacao, Descricao from Organizacao where CodOrganizacao = @codigo", sqlConn);    //Comando SQL DML
                sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                sqlCommand.CommandType = CommandType.Text;

                sqlConn.Open();                                             //abertura a base de dados
                MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                {
                    while (sqlDataReader.Read())                            //Enquanto houver rows
                    {
                        tbNome.Text = sqlDataReader["NomeOrganizacao"].ToString();
                        tbDescricao.Text = sqlDataReader["Descricao"].ToString();
                    }
                }
                else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");
            }
            catch (Exception e)
            {
                MessageBox.Show("Erro BD:\n" + e.Message, "FormCriarOrganizacao - getFieldsData()");
            }
            finally
            {
                sqlConn.Close();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            MySqlConnection sqlConn = Utils.getSqlConnBDLocal();

            //validação de dados
            if (tbCod.Text == "" || tbDescricao.Text == "" || tbNome.Text == "")
            {
                MessageBox.Show("Não são permitidos campos vazios", "Atenção");
            }
            else
            {
                try
                {
                    switch (BDdml)
                    {
                        case "Inserir":

                            if (Utils.getTest()) 
                            {
                                //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                            }

                            MySqlCommand sqlInsert = new MySqlCommand("Insert into Organizacao (NomeOrganizacao, Descricao) VALUES(@NomeOrganizacao, @Descricao)", sqlConn);
                            sqlInsert.Parameters.AddWithValue("@CodOrganizacao", int.Parse(getLastTablePk()));
                            sqlInsert.Parameters.AddWithValue("@NomeOrganizacao", tbNome.Text);
                            sqlInsert.Parameters.AddWithValue("@Descricao", tbDescricao.Text);

                            sqlConn.Open();                                     //abertura da ligaçã ao sgbd
                            sqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL
                            sqlInsert.Transaction = sqlTran;                    //Ligação dos comandos à transação
                            sqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                            sqlTran.Commit();                                   //Commit the transaction

                            MySqlCommand sqlInsertUtilizadorOrganicao =
                                new MySqlCommand("INSERT INTO `Utilizador_Organizacao`(`UtilizadorCodUtilizador`, `OrganizacaoCodOrganizacao`) VALUES (@Utilizador,@Organizacao)", sqlConn);
                            sqlInsertUtilizadorOrganicao.Parameters.AddWithValue("@Utilizador", Global.codigoUtilizador);
                            sqlInsertUtilizadorOrganicao.Parameters.AddWithValue("@Organizacao", tbCod.Text);

                            Console.WriteLine("-->"+sqlInsertUtilizadorOrganicao.CommandText);
                        //abertura da ligaçã ao sgbd
                            sqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL
                            sqlInsertUtilizadorOrganicao.Transaction = sqlTran;                    //Ligação dos comandos à transação
                            sqlInsertUtilizadorOrganicao.ExecuteNonQuery();                        //Executa o SQL DML
                            sqlTran.Commit();   
  
                            break;

                        case "Update":
                            if (Utils.getTest())
                            {
                                MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                            }
                            MySqlCommand sqlUpdate = new MySqlCommand("Update Organizacao SET NomeOrganizacao = @NomeOrganizacao, Descricao = @Descricao WHERE CodOrganizacao = @CodOrganizacao", sqlConn);
                            sqlUpdate.Parameters.AddWithValue("@CodOrganizacao", int.Parse(BDcodigo));
                            sqlUpdate.Parameters.AddWithValue("@NomeOrganizacao", tbNome.Text);
                            sqlUpdate.Parameters.AddWithValue("@Descricao", tbDescricao.Text);
                            
                            sqlConn.Open();                                                 //Abertura a base de dados
                            sqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                            break;

                        case "Delete":
                            if (Utils.getTest())
                            {//MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                            }

                            MySqlCommand sqlDelete = new MySqlCommand("Delete from Organizacao where CodOrganizacao = @CodOrganizacao", sqlConn);
                            sqlDelete.Parameters.AddWithValue("@CodOrganizacao", int.Parse(BDcodigo));

                            sqlConn.Open();                                                 //Abertura a base de dados
                            sqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                            break;

                        default:
                            //MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: FormAluno - Botão OK - switch default");
                            break;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro BD:\n" + ex.Message, "ERRO: FormAluno - Botão OK - switch()");
                    try
                    {
                        sqlTran.Rollback();
                    }
                    catch(Exception exRollback)
                    {
                        MessageBox.Show("Erro BD:\n" + exRollback.Message, "ERRO: FormAluno - Botão OK - switch rollback");
                    }
                }
                finally
                {
                    sqlConn.Close();

                    MessageBox.Show("Procedimento concuido", "INFO");
                    
                    this.Close();
                }
            
            }
                
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
