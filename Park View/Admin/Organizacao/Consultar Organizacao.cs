﻿using MySql.Data.MySqlClient;
using Park_View.Admin.Inicio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Organizacao
{
    public partial class Consulta : Form
    {
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
               BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
               BDcodigo;//do alunos, caso se trate de um update ou insert

        private MySqlConnection sqlConn = null, sqlConn2 = null;

        public Consulta(String sgbd, String codigo)
        {
            InitializeComponent();
            BDsgbd = sgbd;
            BDcodigo = codigo;

            //MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);
        }

        private void Consulta_Load(object sender, EventArgs e)
        {
            sqlConn = Utils.getSqlConnBDLocal();
            sqlConn2 = Utils.getSqlConnBDLocal();

            System.Windows.Forms.ToolTip btnEditToolTip = new System.Windows.Forms.ToolTip();
            btnEditToolTip.SetToolTip(this.btnEdit, "EDITAR");

            System.Windows.Forms.ToolTip btnDeleteToolTip = new System.Windows.Forms.ToolTip();
            btnDeleteToolTip.SetToolTip(this.btnDelete, "APAGAR");

            System.Windows.Forms.ToolTip btnCancelToolTip = new System.Windows.Forms.ToolTip();
            btnCancelToolTip.SetToolTip(this.btnCancel, "VOLTAR");

            //Abre a BD
            if (Utils.getTest())
            {
                //MessageBox.Show("Vou abrir a BD", "TESTES: FormLista_Load()");
            }

            sqlConn.Open();

            if (Utils.getTest())
            {
                //MessageBox.Show("Vou fazer o query", "TESTES: FormLista_Load");
            }

            MySqlCommand sqlCommand = new MySqlCommand("SELECT * FROM `Utilizador_Organizacao` WHERE `UtilizadorCodUtilizador` = " + Global.codigoUtilizador, sqlConn);    //Comando SQL DML
            sqlCommand.CommandType = CommandType.Text;
            MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
           
            while (sqlDataReader.Read())
            {
                sqlConn2.Open();
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter("Select * from Organizacao WHERE `CodOrganizacao`=" + sqlDataReader.GetUInt32("OrganizacaoCodOrganizacao"), sqlConn2);

                if (Utils.getTest())
                {
                    //MessageBox.Show("Vou preenhcer a dataGridView", "TESTES: FormLista_Load");
                }

                DataSet dataRow = new DataSet();
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                Console.WriteLine("cu");

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    DataRow dr = dataTable.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["NomeOrganizacao"].ToString());
                    listitem.SubItems.Add(dr["Descricao"].ToString());
                    Console.WriteLine("nome:" + dr["NomeOrganizacao"].ToString());
                    listViewConsultar.Items.Add(listitem);
                }
                sqlConn2.Close();
            }

            sqlConn.Close();
           
            
            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /*try
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = dataGridView1.Rows[rowIndex];
                textBox1.Text = row.Cells[1].Value.ToString();
                textBox2.Text = row.Cells[0].Value.ToString();

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Erro ArgumentoOutOfRangeException\n" + ex.Message, "Consultar Organizacao - dataGridView_CellClick");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro a tratar\n" + ex.Message, "Consultar Organizacao - dataGridView_CellClick");
            }*/
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                Criar_Organizacao formCriarOrg = new Criar_Organizacao(BDsgbd, "Update", textBox2.Text);
                this.Close();
                formCriarOrg.Show();
            }
            else
            {
                MessageBox.Show("Selecione primeiro um registo");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            PrincipalAdmin princ = new PrincipalAdmin();
            this.Hide();
            princ.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                Criar_Organizacao formCriarOrg = new Criar_Organizacao(BDsgbd, "Delete", textBox2.Text);
                this.Close();
                formCriarOrg.Show();
            }
            else
            {
                MessageBox.Show("Selecione primeiro um registo");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void listViewConsultar_ItemActivate(object sender, EventArgs e)
        {
            
        }
    }
}
