﻿namespace Park_View.Admin.Inicio
{
    partial class PrincipalAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrincipalAdmin));
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menuPaginaPrincipal = new System.Windows.Forms.ToolStripMenuItem();
            this.organizaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.criarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.parqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.criarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.plantaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.criarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.criarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.terminarSessãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.Color.Transparent;
            this.menu.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuPaginaPrincipal,
            this.organizaçãoToolStripMenuItem,
            this.parqueToolStripMenuItem,
            this.plantaToolStripMenuItem,
            this.contasToolStripMenuItem});
            this.menu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menu.Size = new System.Drawing.Size(724, 27);
            this.menu.TabIndex = 2;
            this.menu.Text = "menuStrip1";
            this.menu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menu_ItemClicked);
            // 
            // menuPaginaPrincipal
            // 
            this.menuPaginaPrincipal.ForeColor = System.Drawing.Color.White;
            this.menuPaginaPrincipal.Name = "menuPaginaPrincipal";
            this.menuPaginaPrincipal.Size = new System.Drawing.Size(133, 23);
            this.menuPaginaPrincipal.Text = "Pagina Principal";
            this.menuPaginaPrincipal.Click += new System.EventHandler(this.paginaPrincipalToolStripMenuItem_Click);
            // 
            // organizaçãoToolStripMenuItem
            // 
            this.organizaçãoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.criarToolStripMenuItem2,
            this.consultarToolStripMenuItem2});
            this.organizaçãoToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.organizaçãoToolStripMenuItem.Name = "organizaçãoToolStripMenuItem";
            this.organizaçãoToolStripMenuItem.Size = new System.Drawing.Size(115, 23);
            this.organizaçãoToolStripMenuItem.Text = "Organizações";
            this.organizaçãoToolStripMenuItem.Click += new System.EventHandler(this.organizaçãoToolStripMenuItem_Click);
            // 
            // criarToolStripMenuItem2
            // 
            this.criarToolStripMenuItem2.Name = "criarToolStripMenuItem2";
            this.criarToolStripMenuItem2.Size = new System.Drawing.Size(145, 24);
            this.criarToolStripMenuItem2.Text = "Criar";
            this.criarToolStripMenuItem2.Click += new System.EventHandler(this.criarToolStripMenuItem2_Click);
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(145, 24);
            this.consultarToolStripMenuItem2.Text = "Consultar";
            this.consultarToolStripMenuItem2.Click += new System.EventHandler(this.consultarToolStripMenuItem2_Click);
            // 
            // parqueToolStripMenuItem
            // 
            this.parqueToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.parqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.criarToolStripMenuItem1,
            this.consultarToolStripMenuItem1});
            this.parqueToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.parqueToolStripMenuItem.Name = "parqueToolStripMenuItem";
            this.parqueToolStripMenuItem.Size = new System.Drawing.Size(77, 23);
            this.parqueToolStripMenuItem.Text = "Parques";
            // 
            // criarToolStripMenuItem1
            // 
            this.criarToolStripMenuItem1.Name = "criarToolStripMenuItem1";
            this.criarToolStripMenuItem1.Size = new System.Drawing.Size(145, 24);
            this.criarToolStripMenuItem1.Text = "Criar";
            this.criarToolStripMenuItem1.Click += new System.EventHandler(this.criarToolStripMenuItem1_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(145, 24);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // plantaToolStripMenuItem
            // 
            this.plantaToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.plantaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.criarToolStripMenuItem,
            this.consultarToolStripMenuItem});
            this.plantaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.plantaToolStripMenuItem.Name = "plantaToolStripMenuItem";
            this.plantaToolStripMenuItem.Size = new System.Drawing.Size(71, 23);
            this.plantaToolStripMenuItem.Text = "Plantas";
            this.plantaToolStripMenuItem.MouseHover += new System.EventHandler(this.plantaToolStripMenuItem_MouseHover);
            // 
            // criarToolStripMenuItem
            // 
            this.criarToolStripMenuItem.Name = "criarToolStripMenuItem";
            this.criarToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.criarToolStripMenuItem.Text = "Criar";
            this.criarToolStripMenuItem.Click += new System.EventHandler(this.criarToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // contasToolStripMenuItem
            // 
            this.contasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.criarToolStripMenuItem3,
            this.consultarToolStripMenuItem3,
            this.terminarSessãoToolStripMenuItem});
            this.contasToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.contasToolStripMenuItem.Name = "contasToolStripMenuItem";
            this.contasToolStripMenuItem.Size = new System.Drawing.Size(69, 23);
            this.contasToolStripMenuItem.Text = "Contas";
            // 
            // criarToolStripMenuItem3
            // 
            this.criarToolStripMenuItem3.Name = "criarToolStripMenuItem3";
            this.criarToolStripMenuItem3.Size = new System.Drawing.Size(196, 24);
            this.criarToolStripMenuItem3.Text = "Criar";
            this.criarToolStripMenuItem3.Click += new System.EventHandler(this.criarToolStripMenuItem3_Click);
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(196, 24);
            this.consultarToolStripMenuItem3.Text = "Consultar";
            this.consultarToolStripMenuItem3.Click += new System.EventHandler(this.consultarToolStripMenuItem3_Click);
            // 
            // terminarSessãoToolStripMenuItem
            // 
            this.terminarSessãoToolStripMenuItem.Name = "terminarSessãoToolStripMenuItem";
            this.terminarSessãoToolStripMenuItem.Size = new System.Drawing.Size(196, 24);
            this.terminarSessãoToolStripMenuItem.Text = "Terminar Sessão";
            // 
            // PrincipalAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(724, 404);
            this.Controls.Add(this.menu);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "PrincipalAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrincipalAdmin";
            this.Load += new System.EventHandler(this.PrincipalAdmin_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menuPaginaPrincipal;
        private System.Windows.Forms.ToolStripMenuItem organizaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem criarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem parqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem criarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem plantaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem criarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem criarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem terminarSessãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
    }
}