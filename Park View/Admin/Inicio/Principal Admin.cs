﻿using Park_View.Admin.Contas;
using Park_View.Admin.Inicio;
using Park_View.Admin.Organizacao;
using Park_View.Admin.Parque;
using Park_View.Admin.Planta;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Inicio
{
    public partial class PrincipalAdmin : Form
    {
        public PrincipalAdmin()
        {
            InitializeComponent();
        }

        private void PrincipalAdmin_Load(object sender, EventArgs e)
        {

        }

        private void paginaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Já esta na Pagina Principal");
        }

        private void panelClose_Click(object sender, PaintEventArgs e)
        {
            //this.Close();
        }

        private void panelClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            ActiveForm.WindowState = FormWindowState.Minimized;
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Consulta cons = new Consulta("Organizacao", "Consultar");
            cons.Show();
            this.Hide();
        }

        private void criarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Criar_Organizacao create = new Criar_Organizacao("Organizacao", "Inserir","");
            create.Show();
            //this.Hide();
        }

        private void menu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void criarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Planta.Consultar_Parque planta = new Planta.Consultar_Parque("Planta", "Consultar", "");
            planta.Show();
            //this.Hide();
        }

        private void criarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Criar_Parque parque = new Criar_Parque("Parque", "Inserir", "");
            parque.Show();
            //this.Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Park_View.Admin.Parque.Consultar_Parque parque = new Park_View.Admin.Parque.Consultar_Parque("Parque", "Inserir", "");
            parque.Show();
            this.Hide();
        }

        private void criarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Criar_Conta parque = new Criar_Conta("Conta", "Inserir", "");
            parque.Show();
            //this.Hide();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ConsultarContas parque = new ConsultarContas("Conta", "Inserir");
            parque.Show();
            this.Hide();
        }

        private void panelClose_Paint(object sender, PaintEventArgs e)
        {

        }

        private void organizaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Planta.Consultar_Parque planta = new Planta.Consultar_Parque("Planta", "Consultar","");
            planta.Show();
        }

        private void plantaToolStripMenuItem_MouseHover(object sender, EventArgs e)
        {
            plantaToolStripMenuItem.ForeColor = Color.Black;
        }
    }
}
