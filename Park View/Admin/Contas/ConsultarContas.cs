﻿using MySql.Data.MySqlClient;
using Park_View.Admin.Inicio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Contas
{
    public partial class ConsultarContas : Form
    {
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                  BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                  BDcodigo;//do alunos, caso se trate de um update ou insert

        private MySqlConnection sqlConn = null;

        public ConsultarContas(String sgbd, String codigo)
        {
            InitializeComponent();
            BDsgbd = sgbd;
            BDcodigo = codigo;
        }

        private void ConsultarContas_Load(object sender, EventArgs e)
        {
            sqlConn = Utils.getSqlConnBDLocal();

            System.Windows.Forms.ToolTip btnEditToolTip = new System.Windows.Forms.ToolTip();
            btnEditToolTip.SetToolTip(this.btnEdit, "EDITAR");

            System.Windows.Forms.ToolTip btnDeleteToolTip = new System.Windows.Forms.ToolTip();
            btnDeleteToolTip.SetToolTip(this.btnDelete, "APAGAR");

            System.Windows.Forms.ToolTip btnCancelToolTip = new System.Windows.Forms.ToolTip();
            btnCancelToolTip.SetToolTip(this.btnCancel, "VOLTAR");

            //Abre a BD
            if (Utils.getTest())
            {
                //MessageBox.Show("Vou abrir a BD", "TESTES: FormLista_Load()");
            }

            sqlConn.Open();

            if (Utils.getTest()) {//MessageBox.Show("Vou fazer o query", "TESTES: FormLista_Load");
            }

            MySqlDataAdapter dataAdapter = new MySqlDataAdapter("Select * from Utilizador", sqlConn);

            if (Utils.getTest()) { //MessageBox.Show("Vou preenhcer a dataGridView", "TESTES: FormLista_Load"); 
            }

            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
            sqlConn.Close();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = dataGridView1.Rows[rowIndex];
                textBox1.Text = row.Cells[1].Value.ToString();
                textBox2.Text = row.Cells[0].Value.ToString();

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Erro ArgumentoOutOfRangeException\n" + ex.Message, "Consultar Organizacao - dataGridView_CellClick");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro a tratar\n" + ex.Message, "Consultar Organizacao - dataGridView_CellClick");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "")
            {
                Criar_Conta formCriarOrg = new Criar_Conta(BDsgbd, "Update", textBox2.Text);
                this.Close();
                formCriarOrg.Show();
            }
            else
            {
                MessageBox.Show("Selecione primeiro um registo");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

            PrincipalAdmin princ = new PrincipalAdmin();
            this.Hide();
            princ.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                Criar_Conta formCriarOrg = new Criar_Conta(BDsgbd, "Delete", textBox2.Text);
                this.Close();
                formCriarOrg.Show();
            }
            else
            {
                MessageBox.Show("Selecione primeiro um registo");
            }
        }
    }
}
