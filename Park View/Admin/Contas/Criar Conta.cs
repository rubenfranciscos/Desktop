﻿using Park_View.Admin.Contas;
using Park_View.Admin.Inicio;
using Park_View.Admin.Parque;
using Park_View.Admin.Planta;
using Park_View.Admin.Organizacao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Park_View.Admin
{
    public partial class Criar_Conta : Form
    {
        

        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                       BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                       BDcodigo;//do alunos, caso se trate de um update ou insert

        private MySqlTransaction mysqlTran = null;


        public Criar_Conta(String sgbd, String dml, String codigo)
        {
            InitializeComponent();
            BDsgbd = sgbd;
            BDdml = dml;
            BDcodigo = codigo;
        }

        private void Criar_Conta_Load(object sender, EventArgs e)
        {
            //MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
            //MySqlConnection mysql = Utils.getMySqlConnBDLocalOnline();
            this.Text = BDsgbd + " " + BDdml;
            //this.TipoUtilizadorAdapter.Fill(this.bDlocalDataSet.CicloLetivo);
            System.Windows.Forms.ToolTip btnOKToolTip = new System.Windows.Forms.ToolTip();
            btnOKToolTip.SetToolTip(this.btnOK, "INSERIR");

            System.Windows.Forms.ToolTip btnCancelToolTip = new System.Windows.Forms.ToolTip();
            btnCancelToolTip.SetToolTip(this.btnCncael, "VOLTAR");

            switch (BDdml)
            {
                case "Inserir":
                    tbCod.Text = getLastTablePk();
                    tbNome.Focus();
                    btnOK.BackgroundImage = Properties.Resources.add;
                    break;

                case "Update":
                    getFieldsData();
                    tbCod.Text = BDcodigo;
                    tbNome.Focus();              //Focus nesta caixa
                    btnOK.BackgroundImage = Properties.Resources.edit;
                    break;

                case "Delete":
                    getFieldsData();

                    tbCod.Text = BDcodigo;
                    cbTipoUtilizador.Enabled = false;
                    tbCod.Enabled = false;
                    tbNome.Enabled = false;
                    tbApelido.Enabled = false;
                    tbEmail.Enabled = false;
                    tbMorada.Enabled = false;
                    tbPass.Enabled = false;
                    tbUser.Enabled = false;
                    tbIdade.Enabled = false;
                    
                    btnOK.Focus();

                    btnOK.BackgroundImage = Properties.Resources.delete;
                    break;

            }
        }


        private String getLastTablePk()
        {
            int pkCode = -1;                                                      // Recebe o codigo da tabela a usar nas SQL DML
            //Recebe a UtilsSQl uma ligação ao sgbd
            if (Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
                try
                {

                    if (Utils.getTest())
                    {
                        //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                    }
                    MySqlCommand sqlCommand = new MySqlCommand("Select MAX(CodUtilizador) as CodUtilizador from Utilizador", sqlConn);   //Comando SQL DML
                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                    if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                    {
                        while (sqlDataReader.Read())                             //Enquanto houver rows
                        {
                            String temp = sqlDataReader["CodUtilizador"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                            if (Utils.getTest())
                            {
                                //MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCriar Utilizador");
                            }
                            if (temp == "")
                            {
                                //MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                pkCode = 1;                                     //registo na textbox da codigo par a a

                            }
                            else
                            {
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormConta");
                                }
                                pkCode = int.Parse(temp) + 1;
                            }
                        }
                    }
                    else
                    {
                        // MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                        pkCode = 1;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormConta");

                }
                finally
                {
                    sqlConn.Close();
                }
            }
            else if(Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                try
                {

                    if (Utils.getTest())
                    {
                        //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                    }
                    MySqlCommand sqlCommand = new MySqlCommand("Select MAX(CodUtilizador) as CodUtilizador from Utilizador", sqlConn);   //Comando SQL DML
                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                    if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                    {
                        while (sqlDataReader.Read())                             //Enquanto houver rows
                        {
                            String temp = sqlDataReader["CodUtilizador"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                            if (Utils.getTest())
                            {
                                //MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCriar Utilizador");
                            }
                            if (temp == "")
                            {
                                //MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                pkCode = 1;                                     //registo na textbox da codigo par a a

                            }
                            else
                            {
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormConta");
                                }
                                pkCode = int.Parse(temp) + 1;
                            }
                        }
                    }
                    else
                    {
                        // MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                        pkCode = 1;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormConta");

                }
                finally
                {
                    sqlConn.Close();
                }
            }
            return pkCode.ToString();
        }

  
        private void getFieldsData()
        {
            if(Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
                if (Utils.getTest())
                {
                    //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");
                }
                try
                {
                    MySqlCommand sqlCommand = new MySqlCommand("Select TipoUtilizador, Nome, Apelido, Morada, Password, Username, Idade from Utilizador where CodUtilizador = @codigo", sqlConn);    //Comando SQL DML
                    sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                    if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                    {
                        while (sqlDataReader.Read())                            //Enquanto houver rows
                        {

                            cbTipoUtilizador.Text = sqlDataReader["TipoUtilizador"].ToString();
                            tbNome.Text = sqlDataReader["Nome"].ToString();
                            tbApelido.Text = sqlDataReader["Apelido"].ToString();
                            //tbEmail.Text = sqlDataReader["Descricao"].ToString();
                            tbMorada.Text = sqlDataReader["Morada"].ToString();
                            tbPass.Text = sqlDataReader["Password"].ToString();
                            tbUser.Text = sqlDataReader["Username"].ToString();
                            tbIdade.Text = sqlDataReader["Idade"].ToString();

                        }
                    }
                    else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");
                }
                catch (Exception e)
                {
                    MessageBox.Show("Erro BD:\n" + e.Message, "FormCriarOrganizacao - getFieldsData()");
                }
                finally
                {
                    sqlConn.Close();
                }
            }
            else if(Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                if (Utils.getTest())
                {
                    //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");
                }
                try
                {
                    MySqlCommand sqlCommand = new MySqlCommand("Select TipoUtilizador, Nome, Apelido, Morada, Password, Username, Idade from Utilizador where CodUtilizador = @codigo", sqlConn);    //Comando SQL DML
                    sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                    if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                    {
                        while (sqlDataReader.Read())                            //Enquanto houver rows
                        {

                            cbTipoUtilizador.Text = sqlDataReader["TipoUtilizador"].ToString();
                            tbNome.Text = sqlDataReader["Nome"].ToString();
                            tbApelido.Text = sqlDataReader["Apelido"].ToString();
                            //tbEmail.Text = sqlDataReader["Descricao"].ToString();
                            tbMorada.Text = sqlDataReader["Morada"].ToString();
                            tbPass.Text = sqlDataReader["Password"].ToString();
                            tbUser.Text = sqlDataReader["Username"].ToString();
                            tbIdade.Text = sqlDataReader["Idade"].ToString();

                        }
                    }
                    else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");
                }
                catch (Exception e)
                {
                    MessageBox.Show("Erro BD:\n" + e.Message, "FormCriarOrganizacao - getFieldsData()");
                }
                finally
                {
                    sqlConn.Close();
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();

                //validação de dados
                if (tbCod.Text == "" || tbNome.Text == "" || tbApelido.Text == "" || tbMorada.Text == "" || tbPass.Text == "" || tbUser.Text == "" || cbTipoUtilizador.Text == "")
                {
                    MessageBox.Show("Não são permitidos campos vazios", "Atenção");
                }
                else
                {
                    try
                    {
                        switch (BDdml)
                        {
                            case "Inserir":

                                if (Utils.getTest())
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlInsert = new MySqlCommand("Insert into Utilizador ( Nome, Apelido, Morada, Password, Username, TipoUtilizador, Idade) VALUES(@Nome, @Apelido, @Morada, @Password, @Username, @TipoUtilizador, @Idade)", sqlConn);
                                sqlInsert.Parameters.AddWithValue("@CodUtilizador", int.Parse(getLastTablePk()));
                                sqlInsert.Parameters.AddWithValue("@TipoUtilizador", cbTipoUtilizador.Text);
                                sqlInsert.Parameters.AddWithValue("@Nome", tbNome.Text);
                                sqlInsert.Parameters.AddWithValue("@Apelido", tbApelido.Text);
                                sqlInsert.Parameters.AddWithValue("@Morada", tbMorada.Text);
                                sqlInsert.Parameters.AddWithValue("@Password", tbPass.Text);
                                sqlInsert.Parameters.AddWithValue("@Username", tbUser.Text);
                                sqlInsert.Parameters.AddWithValue("@Idade", tbIdade.Text);

                                sqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                mysqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL
                                sqlInsert.Transaction = mysqlTran;                    //Ligação dos comandos à transação
                                sqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                mysqlTran.Commit();                                   //Commit the transaction

                                break;

                            case "Update":
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlUpdate = new MySqlCommand("Update Utilizador SET Nome = @Nome, Apelido = @Apelido, Morada = @Morada, Password = @Password, Username = @Username, TipoUtilizador = @TipoUtilizador, Idade = @Idade WHERE CodUtilizador = @CodUtilizador", sqlConn);
                                sqlUpdate.Parameters.AddWithValue("@CodUtilizador", int.Parse(BDcodigo));
                                sqlUpdate.Parameters.AddWithValue("@TipoUtilizador", cbTipoUtilizador.Text);
                                sqlUpdate.Parameters.AddWithValue("@Nome", tbNome.Text);
                                sqlUpdate.Parameters.AddWithValue("@Apelido", tbApelido.Text);
                                sqlUpdate.Parameters.AddWithValue("@Morada", tbMorada.Text);
                                sqlUpdate.Parameters.AddWithValue("@Password", tbPass.Text);
                                sqlUpdate.Parameters.AddWithValue("@Username", tbUser.Text);
                                sqlUpdate.Parameters.AddWithValue("@Idade", tbIdade.Text);

                                sqlConn.Open();                                                 //Abertura a base de dados
                                sqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                break;

                            case "Delete":
                                if (Utils.getTest())
                                {
                                    //
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlDelete = new MySqlCommand("Delete from Utilizador where CodUtilizador = @CodUtilizador", sqlConn);
                                sqlDelete.Parameters.AddWithValue("@CodUtilizador", int.Parse(BDcodigo));

                                sqlConn.Open();                                                 //Abertura a base de dados
                                sqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                break;

                            default:
                                MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: FormAluno - Botão OK - switch default");
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro BD:\n" + ex.Message, "ERRO: FormAluno - Botão OK - switch()");
                        try
                        {
                            mysqlTran.Rollback();
                        }
                        catch (Exception exRollback)
                        {
                            MessageBox.Show("Erro BD:\n" + exRollback.Message, "ERRO: FormAluno - Botão OK - switch rollback");
                        }
                    }
                    finally
                    {
                        sqlConn.Close();

                        MessageBox.Show("Procedimento concuido", "INFO");
                        PrincipalAdmin princ = new PrincipalAdmin();

                        this.Close();
                        princ.Show();
                    }

                }
            }
            else if(Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();

                //validação de dados
                if (tbCod.Text == "" || tbNome.Text == "" || tbApelido.Text == "" || tbMorada.Text == "" || tbPass.Text == "" || tbUser.Text == "" || cbTipoUtilizador.Text == "")
                {
                    MessageBox.Show("Não são permitidos campos vazios", "Atenção");
                }
                else
                {
                    try
                    {
                        switch (BDdml)
                        {
                            case "Inserir":

                                if (Utils.getTest())
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlInsert = new MySqlCommand("Insert into Utilizador ( Nome, Apelido, Morada, Password, Username, TipoUtilizador, Idade) VALUES(@Nome, @Apelido, @Morada, @Password, @Username, @TipoUtilizador, @Idade)", sqlConn);
                                sqlInsert.Parameters.AddWithValue("@CodUtilizador", int.Parse(getLastTablePk()));
                                sqlInsert.Parameters.AddWithValue("@TipoUtilizador", cbTipoUtilizador.Text);
                                sqlInsert.Parameters.AddWithValue("@Nome", tbNome.Text);
                                sqlInsert.Parameters.AddWithValue("@Apelido", tbApelido.Text);
                                sqlInsert.Parameters.AddWithValue("@Morada", tbMorada.Text);
                                sqlInsert.Parameters.AddWithValue("@Password", tbPass.Text);
                                sqlInsert.Parameters.AddWithValue("@Username", tbUser.Text);
                                sqlInsert.Parameters.AddWithValue("@Idade", tbIdade.Text);

                                sqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                mysqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL
                                sqlInsert.Transaction = mysqlTran;                    //Ligação dos comandos à transação
                                sqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                mysqlTran.Commit();                                   //Commit the transaction

                                break;

                            case "Update":
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlUpdate = new MySqlCommand("Update Utilizador SET Nome = @Nome, Apelido = @Apelido, Morada = @Morada, Password = @Password, Username = @Username, TipoUtilizador = @TipoUtilizador, Idade = @Idade WHERE CodUtilizador = @CodUtilizador", sqlConn);
                                sqlUpdate.Parameters.AddWithValue("@CodUtilizador", int.Parse(BDcodigo));
                                sqlUpdate.Parameters.AddWithValue("@TipoUtilizador", cbTipoUtilizador.Text);
                                sqlUpdate.Parameters.AddWithValue("@Nome", tbNome.Text);
                                sqlUpdate.Parameters.AddWithValue("@Apelido", tbApelido.Text);
                                sqlUpdate.Parameters.AddWithValue("@Morada", tbMorada.Text);
                                sqlUpdate.Parameters.AddWithValue("@Password", tbPass.Text);
                                sqlUpdate.Parameters.AddWithValue("@Username", tbUser.Text);
                                sqlUpdate.Parameters.AddWithValue("@Idade", tbIdade.Text);

                                sqlConn.Open();                                                 //Abertura a base de dados
                                sqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                break;

                            case "Delete":
                                if (Utils.getTest()) 
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlDelete = new MySqlCommand("Delete from Utilizador where CodUtilizador = @CodUtilizador", sqlConn);
                                sqlDelete.Parameters.AddWithValue("@CodUtilizador", int.Parse(BDcodigo));

                                sqlConn.Open();                                                 //Abertura a base de dados
                                sqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                break;

                            default:
                                MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: FormAluno - Botão OK - switch default");
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro BD:\n" + ex.Message, "ERRO: FormAluno - Botão OK - switch()");
                        try
                        {
                            mysqlTran.Rollback();
                        }
                        catch (Exception exRollback)
                        {
                            MessageBox.Show("Erro BD:\n" + exRollback.Message, "ERRO: FormAluno - Botão OK - switch rollback");
                        }
                    }
                    finally
                    {
                        sqlConn.Close();

                        MessageBox.Show("Procedimento concuido", "INFO");
                        PrincipalAdmin princ = new PrincipalAdmin();

                        this.Close();
                        princ.Show();
                    }

                }
            }
        }

        private void btnCncael_Click(object sender, EventArgs e)
        {
            PrincipalAdmin princ = new PrincipalAdmin();
            this.Hide();
            princ.Show();
        }
    }
}
