﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Planta
{
    class DraggableObject : PictureBox
    {
        private Timer selfTimer;
        private Point startPosition;

        public Boolean active;

        public DraggableObject()
        {
            active = true;
            selfTimer = new Timer();
            selfTimer.Interval = 10;
            selfTimer.Tick += onUpdate;
        }

        public void onUpdate(object sender, EventArgs e)
        {
            if (!active) return;
            Point newPos = new Point(Control.MousePosition.X  - 30, Control.MousePosition.Y - 30);
            this.Location = newPos;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (!active) return;
            base.OnMouseDown(e);
            startPosition = Control.MousePosition;
            startDrag();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (!active) return;
           base.OnMouseUp(e);
           stopDrag();
        }

        public virtual void stopDrag()
        {
            selfTimer.Stop();
        }

        public virtual void startDrag()
        {
            selfTimer.Start();
        }

    }
}
