﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Planta
{
    public partial class Consultar_Parque : Form
    {
        private MySqlConnection sqlConn = null;
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                  BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                  BDcodigo;//do alunos, caso se trate de um update ou insert
        public Consultar_Parque(String sgbd, String dml, String codigo)
        {
            InitializeComponent();

            BDsgbd = sgbd;
            BDcodigo = codigo;
        }

        private void Consultar_Parque_Load(object sender, EventArgs e)
        {
            sqlConn = Utils.getSqlConnBDLocal();


            //Abre a BD
            if (Utils.getTest())
            {
                //MessageBox.Show("Vou abrir a BD", "TESTES: FormLista_Load()");
            }

            sqlConn.Open();

            if (Utils.getTest())
            {
                //MessageBox.Show("Vou fazer o query", "TESTES: FormLista_Load");
            }

            MySqlDataAdapter dataAdapter = new MySqlDataAdapter("Select * from Parque", sqlConn);

            if (Utils.getTest())
            {
                //MessageBox.Show("Vou preenhcer a dataGridView", "TESTES: FormLista_Load");
            }
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
            sqlConn.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = dataGridView1.Rows[rowIndex];
                textBox1.Text = row.Cells[1].Value.ToString();
                textBox2.Text = row.Cells[0].Value.ToString();

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Erro ArgumentoOutOfRangeException\n" + ex.Message, "Consultar Organizacao - dataGridView_CellClick");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro a tratar\n" + ex.Message, "Consultar Organizacao - dataGridView_CellClick");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                Criar_Planta formCriarOrg = new Criar_Planta(BDsgbd, "Insert", textBox2.Text);
                this.Close();
                formCriarOrg.Show();
            }
            else
            {
                MessageBox.Show("Selecione primeiro um registo");
            }
        }
    }
}
