﻿namespace Park_View.Admin.Planta
{
    partial class Criar_Planta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criar_Planta));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCncael = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label7l = new System.Windows.Forms.Label();
            this.pictureBox1l = new System.Windows.Forms.PictureBox();
            this.codPlanta = new System.Windows.Forms.TextBox();
            this.label12l = new System.Windows.Forms.Label();
            this.btnOKº = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.slot15 = new Park_View.Admin.Planta.Slot();
            this.slot14 = new Park_View.Admin.Planta.Slot();
            this.slot13 = new Park_View.Admin.Planta.Slot();
            this.slot12 = new Park_View.Admin.Planta.Slot();
            this.slot10 = new Park_View.Admin.Planta.Slot();
            this.slot9 = new Park_View.Admin.Planta.Slot();
            this.slot4 = new Park_View.Admin.Planta.Slot();
            this.slot8 = new Park_View.Admin.Planta.Slot();
            this.slot7 = new Park_View.Admin.Planta.Slot();
            this.slot6 = new Park_View.Admin.Planta.Slot();
            this.slot5 = new Park_View.Admin.Planta.Slot();
            this.button8 = new Park_View.Admin.Planta.Slot();
            this.slot1 = new Park_View.Admin.Planta.Slot();
            this.slot2 = new Park_View.Admin.Planta.Slot();
            this.button7 = new Park_View.Admin.Planta.Slot();
            this.button3 = new Park_View.Admin.Planta.Slot();
            this.dropArea = new Park_View.Admin.Planta.DroppableTarget();
            this.slot3 = new Park_View.Admin.Planta.Slot();
            this.Atualizar = new System.Windows.Forms.Button();
            this.slot11 = new Park_View.Admin.Planta.Slot();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1l)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot11)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(922, 380);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 22);
            this.label1.TabIndex = 82;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(922, 408);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 22);
            this.label2.TabIndex = 83;
            this.label2.Text = "label2";
            // 
            // btnCncael
            // 
            this.btnCncael.BackColor = System.Drawing.Color.Transparent;
            this.btnCncael.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCncael.BackgroundImage")));
            this.btnCncael.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCncael.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCncael.Location = new System.Drawing.Point(1142, 473);
            this.btnCncael.Name = "btnCncael";
            this.btnCncael.Size = new System.Drawing.Size(48, 44);
            this.btnCncael.TabIndex = 73;
            this.btnCncael.UseVisualStyleBackColor = false;
            this.btnCncael.Click += new System.EventHandler(this.btnCncael_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(1043, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(218, 30);
            this.label3.TabIndex = 87;
            this.label3.Text = "Estacionamentos";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(1085, 270);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 30);
            this.label4.TabIndex = 88;
            this.label4.Text = "Estradas";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(1102, 392);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 30);
            this.label5.TabIndex = 89;
            this.label5.Text = "Lazer";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarToolStripMenuItem,
            this.eliminarToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(118, 48);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.editarToolStripMenuItem.Text = "Editar";
            this.editarToolStripMenuItem.Click += new System.EventHandler(this.editarToolStripMenuItem_Click);
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.eliminarToolStripMenuItem.Text = "Eliminar";
            this.eliminarToolStripMenuItem.Click += new System.EventHandler(this.eliminarToolStripMenuItem_Click);
            // 
            // label7l
            // 
            this.label7l.AutoSize = true;
            this.label7l.BackColor = System.Drawing.Color.Transparent;
            this.label7l.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7l.ForeColor = System.Drawing.Color.White;
            this.label7l.Location = new System.Drawing.Point(388, 9);
            this.label7l.Name = "label7l";
            this.label7l.Size = new System.Drawing.Size(129, 31);
            this.label7l.TabIndex = 94;
            this.label7l.Text = "Criar Planta";
            // 
            // pictureBox1l
            // 
            this.pictureBox1l.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1l.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1l.BackgroundImage")));
            this.pictureBox1l.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1l.Location = new System.Drawing.Point(1023, 53);
            this.pictureBox1l.Name = "pictureBox1l";
            this.pictureBox1l.Size = new System.Drawing.Size(238, 87);
            this.pictureBox1l.TabIndex = 93;
            this.pictureBox1l.TabStop = false;
            // 
            // codPlanta
            // 
            this.codPlanta.Enabled = false;
            this.codPlanta.Location = new System.Drawing.Point(1061, 159);
            this.codPlanta.Name = "codPlanta";
            this.codPlanta.Size = new System.Drawing.Size(211, 20);
            this.codPlanta.TabIndex = 74;
            // 
            // label12l
            // 
            this.label12l.AutoSize = true;
            this.label12l.BackColor = System.Drawing.Color.Transparent;
            this.label12l.ForeColor = System.Drawing.Color.White;
            this.label12l.Location = new System.Drawing.Point(970, 162);
            this.label12l.Name = "label12l";
            this.label12l.Size = new System.Drawing.Size(73, 13);
            this.label12l.TabIndex = 73;
            this.label12l.Text = "Codigo Planta";
            // 
            // btnOKº
            // 
            this.btnOKº.BackColor = System.Drawing.Color.Transparent;
            this.btnOKº.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOKº.BackgroundImage")));
            this.btnOKº.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOKº.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOKº.Location = new System.Drawing.Point(1088, 473);
            this.btnOKº.Name = "btnOKº";
            this.btnOKº.Size = new System.Drawing.Size(48, 44);
            this.btnOKº.TabIndex = 90;
            this.btnOKº.UseVisualStyleBackColor = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // slot15
            // 
            this.slot15.AllowDrop = true;
            this.slot15.BackColor = System.Drawing.Color.Transparent;
            this.slot15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot15.Image = global::Park_View.Properties.Resources.relva;
            this.slot15.Location = new System.Drawing.Point(1125, 425);
            this.slot15.Name = "slot15";
            this.slot15.Size = new System.Drawing.Size(20, 20);
            this.slot15.TabIndex = 86;
            this.slot15.TabStop = false;
            // 
            // slot14
            // 
            this.slot14.AllowDrop = true;
            this.slot14.BackColor = System.Drawing.Color.Transparent;
            this.slot14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot14.Image = global::Park_View.Properties.Resources.estradaVerticalContinuo;
            this.slot14.Location = new System.Drawing.Point(1116, 303);
            this.slot14.Name = "slot14";
            this.slot14.Size = new System.Drawing.Size(20, 20);
            this.slot14.TabIndex = 85;
            this.slot14.TabStop = false;
            // 
            // slot13
            // 
            this.slot13.AllowDrop = true;
            this.slot13.BackColor = System.Drawing.Color.Transparent;
            this.slot13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot13.Image = global::Park_View.Properties.Resources.estradaHorizontalContinuo;
            this.slot13.Location = new System.Drawing.Point(1142, 303);
            this.slot13.Name = "slot13";
            this.slot13.Size = new System.Drawing.Size(20, 20);
            this.slot13.TabIndex = 84;
            this.slot13.TabStop = false;
            // 
            // slot12
            // 
            this.slot12.AllowDrop = true;
            this.slot12.BackColor = System.Drawing.Color.Transparent;
            this.slot12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot12.Image = global::Park_View.Properties.Resources.limpo;
            this.slot12.Location = new System.Drawing.Point(1090, 329);
            this.slot12.Name = "slot12";
            this.slot12.Size = new System.Drawing.Size(20, 20);
            this.slot12.TabIndex = 81;
            this.slot12.TabStop = false;
            // 
            // slot10
            // 
            this.slot10.AllowDrop = true;
            this.slot10.BackColor = System.Drawing.Color.Transparent;
            this.slot10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot10.Image = global::Park_View.Properties.Resources.curvaBaixoDireita;
            this.slot10.Location = new System.Drawing.Point(1125, 355);
            this.slot10.Name = "slot10";
            this.slot10.Size = new System.Drawing.Size(20, 20);
            this.slot10.TabIndex = 79;
            this.slot10.TabStop = false;
            // 
            // slot9
            // 
            this.slot9.AllowDrop = true;
            this.slot9.BackColor = System.Drawing.Color.Transparent;
            this.slot9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot9.Image = global::Park_View.Properties.Resources.curvaCimaDireita;
            this.slot9.Location = new System.Drawing.Point(1142, 329);
            this.slot9.Name = "slot9";
            this.slot9.Size = new System.Drawing.Size(20, 20);
            this.slot9.TabIndex = 78;
            this.slot9.TabStop = false;
            // 
            // slot4
            // 
            this.slot4.AllowDrop = true;
            this.slot4.BackColor = System.Drawing.Color.Transparent;
            this.slot4.Image = global::Park_View.Properties.Resources.lugarDireita;
            this.slot4.Location = new System.Drawing.Point(1168, 225);
            this.slot4.Name = "slot4";
            this.slot4.Size = new System.Drawing.Size(20, 20);
            this.slot4.TabIndex = 77;
            this.slot4.TabStop = false;
            // 
            // slot8
            // 
            this.slot8.AllowDrop = true;
            this.slot8.BackColor = System.Drawing.Color.Transparent;
            this.slot8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot8.Image = global::Park_View.Properties.Resources.curvaBaixoEsquerda;
            this.slot8.Location = new System.Drawing.Point(1168, 329);
            this.slot8.Name = "slot8";
            this.slot8.Size = new System.Drawing.Size(20, 20);
            this.slot8.TabIndex = 74;
            this.slot8.TabStop = false;
            // 
            // slot7
            // 
            this.slot7.AllowDrop = true;
            this.slot7.BackColor = System.Drawing.Color.Transparent;
            this.slot7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot7.Image = global::Park_View.Properties.Resources.curvaCimaEsquerda;
            this.slot7.Location = new System.Drawing.Point(1116, 329);
            this.slot7.Name = "slot7";
            this.slot7.Size = new System.Drawing.Size(20, 20);
            this.slot7.TabIndex = 76;
            this.slot7.TabStop = false;
            // 
            // slot6
            // 
            this.slot6.AllowDrop = true;
            this.slot6.BackColor = System.Drawing.Color.Transparent;
            this.slot6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot6.Image = global::Park_View.Properties.Resources.estradaHorinzontal;
            this.slot6.Location = new System.Drawing.Point(1168, 303);
            this.slot6.Name = "slot6";
            this.slot6.Size = new System.Drawing.Size(20, 20);
            this.slot6.TabIndex = 75;
            this.slot6.TabStop = false;
            // 
            // slot5
            // 
            this.slot5.AllowDrop = true;
            this.slot5.BackColor = System.Drawing.Color.Transparent;
            this.slot5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot5.Image = global::Park_View.Properties.Resources.estradaVertical;
            this.slot5.Location = new System.Drawing.Point(1090, 303);
            this.slot5.Name = "slot5";
            this.slot5.Size = new System.Drawing.Size(20, 20);
            this.slot5.TabIndex = 3;
            this.slot5.TabStop = false;
            // 
            // button8
            // 
            this.button8.AllowDrop = true;
            this.button8.BackColor = System.Drawing.Color.Transparent;
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button8.Location = new System.Drawing.Point(1402, 241);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(67, 55);
            this.button8.TabIndex = 10;
            this.button8.TabStop = false;
            // 
            // slot1
            // 
            this.slot1.AllowDrop = true;
            this.slot1.BackColor = System.Drawing.Color.Transparent;
            this.slot1.Image = global::Park_View.Properties.Resources.lugarBaixo;
            this.slot1.Location = new System.Drawing.Point(1090, 225);
            this.slot1.Name = "slot1";
            this.slot1.Size = new System.Drawing.Size(20, 20);
            this.slot1.TabIndex = 13;
            this.slot1.TabStop = false;
            // 
            // slot2
            // 
            this.slot2.AllowDrop = true;
            this.slot2.BackColor = System.Drawing.Color.Transparent;
            this.slot2.Image = global::Park_View.Properties.Resources.lugarCima;
            this.slot2.Location = new System.Drawing.Point(1116, 225);
            this.slot2.Name = "slot2";
            this.slot2.Size = new System.Drawing.Size(20, 20);
            this.slot2.TabIndex = 14;
            this.slot2.TabStop = false;
            // 
            // button7
            // 
            this.button7.AllowDrop = true;
            this.button7.BackColor = System.Drawing.Color.Transparent;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.Location = new System.Drawing.Point(1402, 137);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(66, 44);
            this.button7.TabIndex = 9;
            this.button7.TabStop = false;
            // 
            // button3
            // 
            this.button3.AllowDrop = true;
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Location = new System.Drawing.Point(1474, 137);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(67, 44);
            this.button3.TabIndex = 4;
            this.button3.TabStop = false;
            // 
            // dropArea
            // 
            this.dropArea.AllowDrop = true;
            this.dropArea.BackColor = System.Drawing.Color.Gray;
            this.dropArea.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.dropArea.Location = new System.Drawing.Point(95, 53);
            this.dropArea.Name = "dropArea";
            this.dropArea.Size = new System.Drawing.Size(800, 600);
            this.dropArea.TabIndex = 3;
            this.dropArea.Paint += new System.Windows.Forms.PaintEventHandler(this.dropArea_Paint);
            this.dropArea.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseClick);
            this.dropArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.dropArea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseUp);
            // 
            // slot3
            // 
            this.slot3.AllowDrop = true;
            this.slot3.BackColor = System.Drawing.Color.Transparent;
            this.slot3.Image = global::Park_View.Properties.Resources.lugarEsquerda;
            this.slot3.Location = new System.Drawing.Point(1142, 225);
            this.slot3.Name = "slot3";
            this.slot3.Size = new System.Drawing.Size(20, 20);
            this.slot3.TabIndex = 18;
            this.slot3.TabStop = false;
            this.slot3.Click += new System.EventHandler(this.slotfgs3_Click);
            // 
            // Atualizar
            // 
            this.Atualizar.Location = new System.Drawing.Point(1100, 523);
            this.Atualizar.Name = "Atualizar";
            this.Atualizar.Size = new System.Drawing.Size(75, 23);
            this.Atualizar.TabIndex = 95;
            this.Atualizar.Text = "Atualizar";
            this.Atualizar.UseVisualStyleBackColor = true;
            // 
            // slot11
            // 
            this.slot11.AllowDrop = true;
            this.slot11.BackColor = System.Drawing.Color.Transparent;
            this.slot11.Image = global::Park_View.Properties.Resources.lugarDiagonalCimaEsquerda;
            this.slot11.Location = new System.Drawing.Point(1194, 225);
            this.slot11.Name = "slot11";
            this.slot11.Size = new System.Drawing.Size(20, 20);
            this.slot11.TabIndex = 96;
            this.slot11.TabStop = false;
            // 
            // Criar_Planta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1350, 680);
            this.Controls.Add(this.slot11);
            this.Controls.Add(this.Atualizar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.codPlanta);
            this.Controls.Add(this.label12l);
            this.Controls.Add(this.label7l);
            this.Controls.Add(this.pictureBox1l);
            this.Controls.Add(this.btnOKº);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.slot15);
            this.Controls.Add(this.slot14);
            this.Controls.Add(this.slot13);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.slot12);
            this.Controls.Add(this.slot10);
            this.Controls.Add(this.slot9);
            this.Controls.Add(this.slot4);
            this.Controls.Add(this.slot8);
            this.Controls.Add(this.slot7);
            this.Controls.Add(this.slot6);
            this.Controls.Add(this.btnCncael);
            this.Controls.Add(this.slot5);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.slot1);
            this.Controls.Add(this.slot2);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.dropArea);
            this.Controls.Add(this.slot3);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Criar_Planta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Criar_Planta";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Criar_Planta_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1l)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slot11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        //Car Slots
        private Slot slot1;
        private Slot button3;
        private Slot slot2;
        private Slot slot5;
       
        private Slot slot3;
        private Slot button7;
        private Slot button8;

        //Other stuff
        private DroppableTarget dropArea;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnCncael;
        private Slot slot8;
        private Slot slot6;
        private Slot slot7;
        private Slot slot4;
        private Slot slot9;
        private Slot slot10;
        private Slot slot12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Slot slot13;
        private Slot slot14;
        private Slot slot15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        private System.Windows.Forms.Label label7l;
        private System.Windows.Forms.PictureBox pictureBox1l;
        private System.Windows.Forms.TextBox codPlanta;
        private System.Windows.Forms.Label label12l;
        private System.Windows.Forms.Button btnOKº;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button Atualizar;
        private Slot slot11;





    }
}