﻿namespace Park_View.Admin.Planta
{
    partial class Alterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Alterar));
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.slot15 = new System.Windows.Forms.Button();
            this.slot14 = new System.Windows.Forms.Button();
            this.slot13 = new System.Windows.Forms.Button();
            this.slot12 = new System.Windows.Forms.Button();
            this.slot10 = new System.Windows.Forms.Button();
            this.slot9 = new System.Windows.Forms.Button();
            this.slot4 = new System.Windows.Forms.Button();
            this.slot8 = new System.Windows.Forms.Button();
            this.slot7 = new System.Windows.Forms.Button();
            this.slot6 = new System.Windows.Forms.Button();
            this.slot5 = new System.Windows.Forms.Button();
            this.slot1 = new System.Windows.Forms.Button();
            this.slot2 = new System.Windows.Forms.Button();
            this.slot3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(154, 305);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 30);
            this.label5.TabIndex = 107;
            this.label5.Text = "Lazer";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(141, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 30);
            this.label4.TabIndex = 106;
            this.label4.Text = "Estradas";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(89, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(218, 30);
            this.label3.TabIndex = 105;
            this.label3.Text = "Estacionamentos";
            // 
            // slot15
            // 
            this.slot15.AllowDrop = true;
            this.slot15.BackColor = System.Drawing.Color.Transparent;
            this.slot15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot15.ForeColor = System.Drawing.Color.White;
            this.slot15.Image = global::Park_View.Properties.Resources.relva;
            this.slot15.Location = new System.Drawing.Point(168, 349);
            this.slot15.Name = "slot15";
            this.slot15.Size = new System.Drawing.Size(40, 40);
            this.slot15.TabIndex = 104;
            this.slot15.TabStop = false;
            this.slot15.UseVisualStyleBackColor = false;
            this.slot15.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot14
            // 
            this.slot14.BackColor = System.Drawing.Color.Transparent;
            this.slot14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot14.ForeColor = System.Drawing.Color.White;
            this.slot14.Image = global::Park_View.Properties.Resources.estradaVerticalContinuo;
            this.slot14.Location = new System.Drawing.Point(140, 160);
            this.slot14.Name = "slot14";
            this.slot14.Size = new System.Drawing.Size(40, 40);
            this.slot14.TabIndex = 103;
            this.slot14.TabStop = false;
            this.slot14.UseVisualStyleBackColor = false;
            this.slot14.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot13
            // 
            this.slot13.BackColor = System.Drawing.Color.Transparent;
            this.slot13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot13.ForeColor = System.Drawing.Color.White;
            this.slot13.Image = global::Park_View.Properties.Resources.estradaHorizontalContinuo;
            this.slot13.Location = new System.Drawing.Point(197, 160);
            this.slot13.Name = "slot13";
            this.slot13.Size = new System.Drawing.Size(40, 40);
            this.slot13.TabIndex = 102;
            this.slot13.TabStop = false;
            this.slot13.UseVisualStyleBackColor = false;
            this.slot13.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot12
            // 
            this.slot12.AllowDrop = true;
            this.slot12.BackColor = System.Drawing.Color.Transparent;
            this.slot12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot12.ForeColor = System.Drawing.Color.White;
            this.slot12.Image = global::Park_View.Properties.Resources.limpo;
            this.slot12.Location = new System.Drawing.Point(83, 206);
            this.slot12.Name = "slot12";
            this.slot12.Size = new System.Drawing.Size(40, 40);
            this.slot12.TabIndex = 101;
            this.slot12.TabStop = false;
            this.slot12.UseVisualStyleBackColor = false;
            this.slot12.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot10
            // 
            this.slot10.AllowDrop = true;
            this.slot10.BackColor = System.Drawing.Color.Transparent;
            this.slot10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot10.ForeColor = System.Drawing.Color.White;
            this.slot10.Image = global::Park_View.Properties.Resources.curvaBaixoDireita;
            this.slot10.Location = new System.Drawing.Point(168, 252);
            this.slot10.Name = "slot10";
            this.slot10.Size = new System.Drawing.Size(40, 40);
            this.slot10.TabIndex = 99;
            this.slot10.TabStop = false;
            this.slot10.UseVisualStyleBackColor = false;
            this.slot10.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot9
            // 
            this.slot9.AllowDrop = true;
            this.slot9.BackColor = System.Drawing.Color.Transparent;
            this.slot9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot9.ForeColor = System.Drawing.Color.White;
            this.slot9.Image = global::Park_View.Properties.Resources.curvaCimaDireita;
            this.slot9.Location = new System.Drawing.Point(197, 206);
            this.slot9.Name = "slot9";
            this.slot9.Size = new System.Drawing.Size(40, 40);
            this.slot9.TabIndex = 98;
            this.slot9.TabStop = false;
            this.slot9.UseVisualStyleBackColor = false;
            this.slot9.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot4
            // 
            this.slot4.AllowDrop = true;
            this.slot4.BackColor = System.Drawing.Color.Transparent;
            this.slot4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot4.ForeColor = System.Drawing.Color.Transparent;
            this.slot4.Image = global::Park_View.Properties.Resources.lugarDireita;
            this.slot4.Location = new System.Drawing.Point(197, 70);
            this.slot4.Name = "slot4";
            this.slot4.Size = new System.Drawing.Size(40, 40);
            this.slot4.TabIndex = 97;
            this.slot4.TabStop = false;
            this.slot4.UseVisualStyleBackColor = false;
            this.slot4.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot8
            // 
            this.slot8.AllowDrop = true;
            this.slot8.BackColor = System.Drawing.Color.Transparent;
            this.slot8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot8.ForeColor = System.Drawing.Color.White;
            this.slot8.Image = global::Park_View.Properties.Resources.curvaBaixoEsquerda;
            this.slot8.Location = new System.Drawing.Point(254, 206);
            this.slot8.Name = "slot8";
            this.slot8.Size = new System.Drawing.Size(40, 40);
            this.slot8.TabIndex = 94;
            this.slot8.TabStop = false;
            this.slot8.UseVisualStyleBackColor = false;
            this.slot8.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot7
            // 
            this.slot7.AllowDrop = true;
            this.slot7.BackColor = System.Drawing.Color.Transparent;
            this.slot7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot7.ForeColor = System.Drawing.Color.White;
            this.slot7.Image = global::Park_View.Properties.Resources.curvaCimaEsquerda;
            this.slot7.Location = new System.Drawing.Point(140, 206);
            this.slot7.Name = "slot7";
            this.slot7.Size = new System.Drawing.Size(40, 40);
            this.slot7.TabIndex = 96;
            this.slot7.TabStop = false;
            this.slot7.UseVisualStyleBackColor = false;
            this.slot7.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot6
            // 
            this.slot6.AllowDrop = true;
            this.slot6.BackColor = System.Drawing.Color.Transparent;
            this.slot6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot6.ForeColor = System.Drawing.Color.White;
            this.slot6.Image = global::Park_View.Properties.Resources.estradaHorinzontal;
            this.slot6.Location = new System.Drawing.Point(254, 160);
            this.slot6.Name = "slot6";
            this.slot6.Size = new System.Drawing.Size(40, 40);
            this.slot6.TabIndex = 95;
            this.slot6.TabStop = false;
            this.slot6.UseVisualStyleBackColor = false;
            this.slot6.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot5
            // 
            this.slot5.AllowDrop = true;
            this.slot5.BackColor = System.Drawing.Color.Transparent;
            this.slot5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slot5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot5.ForeColor = System.Drawing.Color.White;
            this.slot5.Image = global::Park_View.Properties.Resources.estradaVertical;
            this.slot5.Location = new System.Drawing.Point(83, 160);
            this.slot5.Name = "slot5";
            this.slot5.Size = new System.Drawing.Size(40, 40);
            this.slot5.TabIndex = 90;
            this.slot5.TabStop = false;
            this.slot5.UseVisualStyleBackColor = false;
            this.slot5.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot1
            // 
            this.slot1.AllowDrop = true;
            this.slot1.BackColor = System.Drawing.Color.Transparent;
            this.slot1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot1.ForeColor = System.Drawing.Color.Transparent;
            this.slot1.Image = global::Park_View.Properties.Resources.lugarBaixo;
            this.slot1.Location = new System.Drawing.Point(83, 70);
            this.slot1.Name = "slot1";
            this.slot1.Size = new System.Drawing.Size(40, 40);
            this.slot1.TabIndex = 91;
            this.slot1.TabStop = false;
            this.slot1.UseVisualStyleBackColor = false;
            this.slot1.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot2
            // 
            this.slot2.AllowDrop = true;
            this.slot2.BackColor = System.Drawing.Color.Transparent;
            this.slot2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot2.ForeColor = System.Drawing.Color.Transparent;
            this.slot2.Image = global::Park_View.Properties.Resources.lugarCima;
            this.slot2.Location = new System.Drawing.Point(140, 70);
            this.slot2.Name = "slot2";
            this.slot2.Size = new System.Drawing.Size(40, 40);
            this.slot2.TabIndex = 92;
            this.slot2.TabStop = false;
            this.slot2.UseVisualStyleBackColor = false;
            this.slot2.Click += new System.EventHandler(this.slot1_Click);
            // 
            // slot3
            // 
            this.slot3.AllowDrop = true;
            this.slot3.BackColor = System.Drawing.Color.Transparent;
            this.slot3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.slot3.ForeColor = System.Drawing.Color.Transparent;
            this.slot3.Image = global::Park_View.Properties.Resources.lugarBaixo;
            this.slot3.Location = new System.Drawing.Point(254, 70);
            this.slot3.Name = "slot3";
            this.slot3.Size = new System.Drawing.Size(40, 40);
            this.slot3.TabIndex = 93;
            this.slot3.TabStop = false;
            this.slot3.UseVisualStyleBackColor = false;
            this.slot3.Click += new System.EventHandler(this.slot1_Click);
            // 
            // Alterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(370, 425);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.slot15);
            this.Controls.Add(this.slot14);
            this.Controls.Add(this.slot13);
            this.Controls.Add(this.slot12);
            this.Controls.Add(this.slot10);
            this.Controls.Add(this.slot9);
            this.Controls.Add(this.slot4);
            this.Controls.Add(this.slot8);
            this.Controls.Add(this.slot7);
            this.Controls.Add(this.slot6);
            this.Controls.Add(this.slot5);
            this.Controls.Add(this.slot1);
            this.Controls.Add(this.slot2);
            this.Controls.Add(this.slot3);
            this.Name = "Alterar";
            this.Text = "Alterar";
            this.Load += new System.EventHandler(this.Alterar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button slot15;
        private System.Windows.Forms.Button slot14;
        private System.Windows.Forms.Button slot13;
        private System.Windows.Forms.Button slot12;
        private System.Windows.Forms.Button slot10;
        private System.Windows.Forms.Button slot9;
        private System.Windows.Forms.Button slot4;
        private System.Windows.Forms.Button slot8;
        private System.Windows.Forms.Button slot7;
        private System.Windows.Forms.Button slot6;
        private System.Windows.Forms.Button slot5;
        private System.Windows.Forms.Button slot1;
        private System.Windows.Forms.Button slot2;
        private System.Windows.Forms.Button slot3;
    }
}