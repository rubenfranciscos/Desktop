﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Planta
{
    public partial class Alterar : Form
    {
        private Slot carroSeleciondo;
        internal Alterar(Slot carro)
        {
            InitializeComponent();
            carroSeleciondo = carro;
        }

        private void Alterar_Load(object sender, EventArgs e)
        {

        }

        private void alterarCarro(int x, int y, string nome)
        {
            MySqlConnection sqlConn = Utils.getSqlConnBDLocal();

            MySqlCommand sqlUpdate = new MySqlCommand("UPDATE `LugarParque` SET `Nome`='" 
                + nome + "' WHERE `X`=" + x + " AND `Y`=" + y, sqlConn);

            sqlUpdate.CommandType = CommandType.Text;
            Console.WriteLine("UPDATE `LugarParque` SET `Nome`='"
                + nome + "' WHERE `X`=" + x + " AND `Y`=" + y);
            sqlConn.Open();
            sqlUpdate.ExecuteNonQuery();
        }

        private void slot1_Click(object sender, EventArgs e)
        {
            //carroSeleciondo.Hide();//BackgroundImage = ((Slot)sender).BackgroundImage;
            carroSeleciondo.Image = ((Button)sender).Image;
            carroSeleciondo.BackgroundImage = ((Button)sender).BackgroundImage;
            carroSeleciondo.Invalidate();


            alterarCarro(carroSeleciondo.Location.X,carroSeleciondo.Location.Y,((Button)sender).Name);
        }
    }
}
