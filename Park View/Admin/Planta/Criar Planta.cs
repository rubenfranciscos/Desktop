﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Planta
{
    public partial class Criar_Planta : Form
    {

        /*private static Dictionary<int, Carro> ListaCarros = new Dictionary<int, Carro>();
        private static Dictionary<int, Label> listaLabelCarros = new Dictionary<int, Label>();
        */
        private Point firstPoint = new Point();
        Panel carroMouse = new Panel();
        static Slot carroClicado;
        static Slot carroSelecionado;
        int X = 0, Y = 0;

        String codigo;
        List<Panel> listaCarros = new List<Panel>();
        private MySqlConnection sqlConn = null;

        public Criar_Planta(String sgbd, String dml, String codigo)
        {
            InitializeComponent();

            this.codigo = codigo;
            
            codPlanta.Text = getLastTablePk();
            codPlanta.Enabled = false;

            //label1.Text("x: " + MousePosition.X.ToString);
            slot1.onStopDrag += slot1_onStopDrag;
            slot2.onStopDrag += slot2_onStopDrag;
            slot3.onStopDrag += slot3_onStopDrag;
            slot3.BringToFront();
            slot4.onStopDrag += slot4_onStopDrag;
            slot5.onStopDrag += slot5_onStopDrag;
            slot6.onStopDrag += slot6_onStopDrag;
            slot7.onStopDrag += slot7_onStopDrag;
            slot8.onStopDrag += slot8_onStopDrag;
            slot9.onStopDrag += slot9_onStopDrag;
            slot10.onStopDrag += slot10_onStopDrag;
            slot11.onStopDrag += slot16_onStopDrag;
            slot12.onStopDrag += slot12_onStopDrag;
            slot13.onStopDrag += slot13_onStopDrag;
            slot14.onStopDrag += slot14_onStopDrag;
            slot15.onStopDrag += slot15_onStopDrag;
            
        }




        public Criar_Planta()
        {
            // TODO: Complete member initialization
        }

        private void inserirCarro(int x, int y, string nome){

            if (Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();

                MySqlCommand sqlCommand = new MySqlCommand("Insert into `LugarParque` (`CodLugarParque`, `ParqueCodParque`, `Estado`, `Nome`, `X`, `Y`) VALUES ("
                    + codPlanta.Text + " , "
                    + Int32.Parse(codigo) + ", 0 , '"
                    + nome + "' , "
                    + x + " , "
                    + y + ");", sqlConn);   //Comando SQL DML
                sqlCommand.CommandType = CommandType.Text;
                Console.WriteLine("Insert into `LugarParque` (`CodLugarParque`, `ParqueCodParque`, `Estado`, `Nome`, `X`, `Y`) VALUES (" + codPlanta.Text + " , " + Int32.Parse(codigo) + ", 0 , '" + nome + "' , " + x + " , " + y + ");");
                sqlConn.Open();
                sqlCommand.ExecuteNonQuery();

            }
            else if (Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();

                MySqlCommand sqlCommand = new MySqlCommand("Insert into `LugarParque` (`CodLugarParque`, `ParqueCodParque`, `Estado`, `Nome`, `X`, `Y`) VALUES ("
                    + codPlanta.Text + " , "
                    + Int32.Parse(codigo) + ", 0 , '"
                    + nome + "' , "
                    + x + " , "
                    + y + ");", sqlConn);   //Comando SQL DML
                sqlCommand.CommandType = CommandType.Text;
                Console.WriteLine("Insert into `LugarParque` (`CodLugarParque`, `ParqueCodParque`, `Estado`, `Nome`, `X`, `Y`) VALUES (" + codPlanta.Text + " , " + Int32.Parse(codigo) + ", 0 , '" + nome + "' , " + x + " , " + y + ");");
                sqlConn.Open();
                sqlCommand.ExecuteNonQuery();
            }
        }



        private void eliminarCarro(int x, int y, string nome)
        {

            if (Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();

                MySqlCommand sqlCommand = new MySqlCommand("Insert into `LugarParque` (`CodLugarParque`, `ParqueCodParque`, `Estado`, `Nome`, `X`, `Y`) VALUES ("
                    + codPlanta.Text + " , "
                    + Int32.Parse(codigo) + ", 0 , '"
                    + nome + "' , "
                    + x + " , "
                    + y + ");", sqlConn);   //Comando SQL DML
                sqlCommand.CommandType = CommandType.Text;
                Console.WriteLine("Insert into `LugarParque` (`CodLugarParque`, `ParqueCodParque`, `Estado`, `Nome`, `X`, `Y`) VALUES (" + codPlanta.Text + " , " + Int32.Parse(codigo) + ", 0 , '" + nome + "' , " + x + " , " + y + ");");
                sqlConn.Open();
                sqlCommand.ExecuteNonQuery();

            }
            else if (Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();

                MySqlCommand sqlCommand = new MySqlCommand("Insert into `LugarParque` (`CodLugarParque`, `ParqueCodParque`, `Estado`, `Nome`, `X`, `Y`) VALUES ("
                    + codPlanta.Text + " , "
                    + Int32.Parse(codigo) + ", 0 , '"
                    + nome + "' , "
                    + x + " , "
                    + y + ");", sqlConn);   //Comando SQL DML
                sqlCommand.CommandType = CommandType.Text;
                Console.WriteLine("Insert into `LugarParque` (`CodLugarParque`, `ParqueCodParque`, `Estado`, `Nome`, `X`, `Y`) VALUES (" + codPlanta.Text + " , " + Int32.Parse(codigo) + ", 0 , '" + nome + "' , " + x + " , " + y + ");");
                sqlConn.Open();
                sqlCommand.ExecuteNonQuery();
            }
            
        }


        private String getLastTablePk()
        {
            
            int pkCode = -1;                                                      // Recebe o codigo da tabela a usar nas SQL DML
            //Recebe a UtilsSQl uma ligação ao sgbd
            if(Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
                try
                {

                    if (Utils.getTest())
                    {
                        //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                    }
                    MySqlCommand sqlCommand = new MySqlCommand("Select MAX(CodLugarParque) as CodLugarParque from LugarParque", sqlConn);   //Comando SQL DML
                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                    if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                    {
                        while (sqlDataReader.Read())                             //Enquanto houver rows
                        {
                            String temp = sqlDataReader["CodLugarParque"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                            if (Utils.getTest())
                            {
                                //MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCriar Utilizador");
                            }
                            if (temp == "")
                            {
                                //MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                pkCode = 1;                                     //registo na textbox da codigo par a a

                            }
                            else
                            {
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormConta");
                                }
                                pkCode = int.Parse(temp) + 1;
                            }
                        }
                    }
                    else
                    {
                        // MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                        pkCode = 1;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormConta");

                }
                finally
                {
                    sqlConn.Close();
                }
            }
            else if(Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                try
                {

                    if (Utils.getTest())
                    {
                        //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                    }
                    MySqlCommand sqlCommand = new MySqlCommand("Select MAX(CodLugarParque) as CodLugarParque from LugarParque", sqlConn);   //Comando SQL DML
                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                    if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                    {
                        while (sqlDataReader.Read())                             //Enquanto houver rows
                        {
                            String temp = sqlDataReader["CodLugarParque"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                            if (Utils.getTest())
                            {
                                //MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCriar Utilizador");
                            }
                            if (temp == "")
                            {
                                //MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                pkCode = 1;                                     //registo na textbox da codigo par a a

                            }
                            else
                            {
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormConta");
                                }
                                pkCode = int.Parse(temp) + 1;
                            }
                        }
                    }
                    else
                    {
                        // MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                        pkCode = 1;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormConta");

                }
                finally
                {
                    sqlConn.Close();
                }
                
            }
            return pkCode.ToString();
        }

        
        void slot1_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro = new Slot();
            carro.Name = "LugarBaixo";
            carro.Location = dropArea.getNextAvailableLocation(slot1);
           //carro.active = false;
            carro.Image = slot1.Image;
            carro.BackColor = slot1.BackColor;
            carro.Width = slot1.Width;
            carro.Height = slot1.Height;

            //MessageBox.Show("Nome - " + carro.Name + "\nLocalizacao - " + carro.Location);

            dropArea.Controls.Add(carro);
            carro.active = false;

            carro.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            
            //Reset slot 
            slot1.Location = slot1.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro.Location.X, carro.Location.Y, carro.Name);

            codPlanta.Text = getLastTablePk();
        }

        void slot2_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro2 = new Slot();
            carro2.Name = "LugarCima";
            carro2.Location = dropArea.getNextAvailableLocation(slot2);
            //carro.active = false;
            carro2.Image = slot2.Image;
            carro2.BackColor = slot2.BackColor;
            carro2.Width = slot2.Width;
            carro2.Height = slot2.Height;
            //MessageBox.Show("Nome - " + carro2.Name + "\nLocalizacao - " + carro2.Location);

            dropArea.Controls.Add(carro2);

            carro2.active = false;

            carro2.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro2;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot2.Location = slot2.initLocation;
            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro2.Location.X, carro2.Location.Y, carro2.Name);

            codPlanta.Text = getLastTablePk();
        }

        void slot3_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro3 = new Slot();
            carro3.Name = "LugarEsquerda";
            carro3.Location = dropArea.getNextAvailableLocation(slot3);
            //carro.active = false;
            carro3.Image = slot3.Image;
            carro3.BackColor = slot3.BackColor;
            carro3.Width = slot3.Width;
            carro3.Height = slot3.Height;


            carro3.active = false;

            carro3.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro3;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

           // MessageBox.Show("Nome - " + carro3.Name + "\nLocalizacao - " + carro3.Location);

            dropArea.Controls.Add(carro3);

            //Reset slot 
            slot3.Location = slot3.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro3.Location.X, carro3.Location.Y, carro3.Name);

            codPlanta.Text = getLastTablePk();
        }


        private void slot4_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro4 = new Slot();
            carro4.Name = "LugarDireita";
            carro4.Location = dropArea.getNextAvailableLocation(slot4);
            //carro.active = false;
            carro4.Image = slot4.Image;
            carro4.BackColor = slot4.BackColor;
            carro4.Width = slot4.Width;
            carro4.Height = slot4.Height;

            //MessageBox.Show("Nome - " + carro4.Name + "\nLocalizacao - " + carro4.Location);

            dropArea.Controls.Add(carro4);
            carro4.active = false;

            carro4.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro4;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot4.Location = slot4.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro4.Location.X, carro4.Location.Y, carro4.Name);

            codPlanta.Text = getLastTablePk();
        }


        private void slot5_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro5 = new Slot();
            carro5.Name = "estradaVertical";
            carro5.Location = dropArea.getNextAvailableLocation(slot5);
            //carro.active = false;
            carro5.Image = slot5.Image;
            carro5.BackColor = slot5.BackColor;
            carro5.Width = slot5.Width;
            carro5.Height = slot5.Height;
            //MessageBox.Show("Nome - " + carro5.Name + "\nLocalizacao - " + carro5.Location);

            dropArea.Controls.Add(carro5);
            carro5.active = false;

            carro5.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro5;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot5.Location = slot5.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro5.Location.X, carro5.Location.Y, carro5.Name);

            codPlanta.Text = getLastTablePk();
        }

        private void slot6_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro6 = new Slot();
            carro6.Name = "estradaHorizontal";
            carro6.Location = dropArea.getNextAvailableLocation(slot6);
            //carro.active = false;
            carro6.Image = slot6.Image;
            carro6.BackColor = Color.Red;
            carro6.Width = slot6.Width;
            carro6.Height = slot6.Height;

            //MessageBox.Show("Nome - " + carro6.Name + "\nLocalizacao - " + carro6.Location);

            dropArea.Controls.Add(carro6);
            carro6.active = false;

            carro6.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro6;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot6.Location = slot6.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro6.Location.X, carro6.Location.Y, carro6.Name);

            codPlanta.Text = getLastTablePk();
        }

        private void slot7_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro7 = new Slot();
            carro7.Name = "estradaCimaEsquerda";
            carro7.Location = dropArea.getNextAvailableLocation(slot7);
            //carro.active = false;
            carro7.Image = slot7.Image;
            carro7.BackColor = Color.Red;
            carro7.Width = slot7.Width;
            carro7.Height = slot6.Height;
            //MessageBox.Show("Nome - " + carro7.Name + "\nLocalizacao - " + carro7.Location);

            dropArea.Controls.Add(carro7);

            carro7.active = false;

            carro7.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro7;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot7.Location = slot7.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro7.Location.X, carro7.Location.Y, carro7.Name);

            codPlanta.Text = getLastTablePk();
        }

        private void slot8_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro8 = new Slot();
            carro8.Name = "estradaBaixoEsquerda";
            carro8.Location = dropArea.getNextAvailableLocation(slot8);
            //carro.active = false;
            carro8.Image = slot8.Image;
            carro8.BackColor = Color.Red;
            carro8.Width = slot8.Width;
            carro8.Height = slot8.Height;
            //MessageBox.Show("Nome - " + carro8.Name + "\nLocalizacao - " + carro8.Location);

            dropArea.Controls.Add(carro8);

            //Reset slot 
            slot8.Location = slot8.initLocation;

            carro8.active = false;
            carro8.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro8;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro8.Location.X, carro8.Location.Y, carro8.Name);

            codPlanta.Text = getLastTablePk();
        }

        private void slot9_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro9 = new Slot();
            carro9.Name = "estradaCimaDireita";
            carro9.Location = dropArea.getNextAvailableLocation(slot9);
            //carro.active = false;
            carro9.Image = slot9.Image;
            carro9.BackColor = Color.Red;
            carro9.Width = slot9.Width;
            carro9.Height = slot9.Height;
            //MessageBox.Show("Nome - " + carro9.Name + "\nLocalizacao - " + carro9.Location);

            dropArea.Controls.Add(carro9);

            //Reset slot 
            slot9.Location = slot9.initLocation;

            carro9.active = false;

            carro9.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro9;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro9.Location.X, carro9.Location.Y, carro9.Name);

            codPlanta.Text = getLastTablePk();
        }

        private void slot10_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro10 = new Slot();
            carro10.Name = "estradaBaixoDireita";
            carro10.Location = dropArea.getNextAvailableLocation(slot10);
            //carro.active = false;
            carro10.Image = slot10.Image;
            carro10.BackColor = Color.Red;
            carro10.Width = slot10.Width;
            carro10.Height = slot10.Height;

            //MessageBox.Show("Nome - " + carro10.Name + "\nLocalizacao - " + carro10.Location);

            dropArea.Controls.Add(carro10);

            carro10.active = false;

            carro10.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro10;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot10.Location = slot10.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro10.Location.X, carro10.Location.Y, carro10.Name);

            codPlanta.Text = getLastTablePk();
        }


        private void slot12_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro12 = new Slot();
            carro12.Name = "limpo";
            carro12.Location = dropArea.getNextAvailableLocation(slot12);
            //carro.active = false;
            carro12.Image = slot12.Image;
            carro12.BackColor = Color.Red;
            carro12.Width = slot12.Width;
            carro12.Height = slot12.Height;

            //MessageBox.Show("Nome - " + carro12.Name + "\nLocalizacao - " + carro12.Location);
            dropArea.Controls.Add(carro12);

            carro12.active = false;

            carro12.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro12;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot12.Location = slot12.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro12.Location.X, carro12.Location.Y, carro12.Name);

            codPlanta.Text = getLastTablePk();
        }

        private void slot13_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro13 = new Slot();
            carro13.Name = "estradaHorizontalContinuo";
            carro13.Location = dropArea.getNextAvailableLocation(slot13);
            //carro.active = false;
            carro13.Image = slot13.Image;
            carro13.BackColor = Color.Red;
            carro13.Width = slot13.Width;
            carro13.Height = slot13.Height;

           // MessageBox.Show("Nome - " + carro13.Name + "\nLocalizacao - " + carro13.Location);
            dropArea.Controls.Add(carro13);


            carro13.active = false;

            carro13.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro13;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot13.Location = slot13.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro13.Location.X, carro13.Location.Y, carro13.Name);

            codPlanta.Text = getLastTablePk();
        }

        private void slot14_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro14 = new Slot();
            carro14.Name = "estrdaVerticalContinuo";
            carro14.Location = dropArea.getNextAvailableLocation(slot14);
            //carro.active = false;
            carro14.Image = slot14.Image;
            carro14.BackColor = Color.Red;
            carro14.Width = slot14.Width;
            carro14.Height = slot14.Height;

            //MessageBox.Show("Nome - " + carro14.Name + "\nLocalizacao - " + carro14.Location);
            dropArea.Controls.Add(carro14);

            carro14.active = false;

            carro14.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro14;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot14.Location = slot14.initLocation;

            dropArea.BackColor = Color.DimGray;

            dropArea.BackgroundImage = null;

            inserirCarro(carro14.Location.X, carro14.Location.Y, carro14.Name);

            codPlanta.Text = getLastTablePk();
        }

        private void slot15_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro15 = new Slot();
            carro15.Name = "relva";
            carro15.Location = dropArea.getNextAvailableLocation(slot15);
            //carro.active = false;
            carro15.Image = slot15.Image;
            carro15.BackColor = Color.Red;
            carro15.Width = slot15.Width;
            carro15.Height = slot15.Height;

            //MessageBox.Show("Nome - " + carro15.Name + "\nLocalizacao - " + carro15.Location);
            dropArea.Controls.Add(carro15);

            carro15.active = false;

            carro15.MouseClick += ((o, a) => {
                if(a.Button.ToString() == "Right")
                {
                    carroClicado = carro15;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
                else
                {

                }
            });
            //Reset slot 
            slot15.Location = slot15.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro15.Location.X, carro15.Location.Y, carro15.Name);

            codPlanta.Text = getLastTablePk();

        }
        


        private void slot16_onStopDrag(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro16 = new Slot();
            carro16.Name = "lugarDiagonalCimaEsquerda";
            carro16.Location = dropArea.getNextAvailableLocation(slot11);
            //carro.active = false;
            carro16.Image = slot11.Image;
            carro16.BackColor = Color.Red;
            carro16.Width = slot11.Width;
            carro16.Height = slot11.Height;

            //MessageBox.Show("Nome - " + carro15.Name + "\nLocalizacao - " + carro15.Location);
            dropArea.Controls.Add(carro16);

            carro16.active = false;

            carro16.MouseClick += ((o, a) =>
            {
                if(a.Button.ToString() == "Right")
                {
                    carroClicado = slot11;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
                else
                {

                }
            });
            //Reset slot 
            slot11.Location = slot11.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            inserirCarro(carro16.Location.X, carro16.Location.Y, carro16.Name);

            codPlanta.Text = getLastTablePk();

        }
        private void panelClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            label1.Text = "X:" + e.X;
            label2.Text = "Y:" + e.Y;

            X = e.X;
            Y = e.Y;

            carroMouse.Location = new Point(e.X, e.Y);
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            listaCarros.Add(carroMouse);
            carroMouse = new Panel();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /*Panel carro = new Panel();
            carro.BackgroundImage = Properties.Resources.cima_livre;
            carro.BackgroundImageLayout = ImageLayout.Stretch;
            carro.Size = new Size(35,55);
            carro.Location = new Point(100,100);

            dropArea.Controls.Add(carro);

            MessageBox.Show("olaaa");
            carro.MouseClick += panel2_MouseClick;
            carro.MouseDown += carro_Mover;

            //carroMouse = carro;*/

        }

        private void carro_Mover(object sender, MouseEventArgs e)
        {
            //labelX.Text = "X:" + e.X;
         //   labelY.Text = "Y:" + e.Y;
            //((Panel)sender).Location = new Point(e.X, e.Y);
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            //listaCarros.Add(carroMouse);
            //carroMouse = new Panel();
        }

        private void Criar_Planta_Load(object sender, EventArgs e)
        {

            timer1.Start();

        }

        
        private void btnCncael_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void slotfgs3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            carroClicado.Hide();

            MySqlConnection sqlConn = Utils.getSqlConnBDLocal();

            MySqlCommand sqlUpdate = new MySqlCommand("DELETE FROM `LugarParque` WHERE `X`=" + carroClicado.Location.X + " AND `Y`=" + carroClicado.Location.Y + " AND `ParqueCodParque`=" + codigo, sqlConn);

            sqlUpdate.CommandType = CommandType.Text;
            Console.WriteLine("DELETE FROM `LugarParque` WHERE `X`=" + carroClicado.Location.X + " AND `Y`=" + carroClicado.Location.Y + " AND `ParqueCodParque`=" + codigo);
            sqlConn.Open();
            sqlUpdate.ExecuteNonQuery();
            
        }

        private void editarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alterar alterar = new Alterar(carroClicado);
            alterar.Show();
        }

        private void cbTipoUtilizadlor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            dropArea.Controls.Clear();
            if (Global.check == true)
            {

                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                try
                {
                    MySqlCommand sqlCommand = new MySqlCommand("SELECT * FROM `LugarParque` WHERE `ParqueCodParque` = " + codigo, sqlConn);    //Comando SQL DML

                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD

                    while (sqlDataReader.Read())
                    {
                        Slot novoSlot = new Slot();
                        novoSlot.Name = sqlDataReader.GetString("Nome");
                        novoSlot.Location = new Point(sqlDataReader.GetInt32("X"), sqlDataReader.GetInt32("Y"));
                        novoSlot.estado = sqlDataReader.GetInt32("Estado");
                        novoSlot.Image = slot5.Image;
                        novoSlot.BackColor = slot5.BackColor;
                        novoSlot.Width = slot5.Width;
                        novoSlot.Height = slot5.Height;
                        novoSlot.active = false;
                        novoSlot.MouseClick += ((o, a) =>
                        {
                            if (a.Button.ToString() == "Right")
                            {
                                carroClicado = novoSlot;
                                contextMenuStrip1.Show(Control.MousePosition);
                            }
                        });

                        dropArea.Controls.Add(novoSlot);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro BD:\n" + ex.Message, "Login - getFieldsData()");
                }
                finally
                {
                    sqlConn.Close();
                }

            }
            else
            {
                if (Global.checkPI == true)
                {
                    MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
                    try
                    {
                        MySqlCommand sqlCommand = new MySqlCommand("SELECT * FROM `LugarParque` WHERE `ParqueCodParque` = " + codigo, sqlConn);    //Comando SQL DML

                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD

                        while (sqlDataReader.Read())
                        {
                            Slot novoSlot = new Slot();
                            novoSlot.Name = sqlDataReader.GetString("Nome");
                            novoSlot.Location = new Point(sqlDataReader.GetInt32("X"), sqlDataReader.GetInt32("Y"));
                            novoSlot.estado = sqlDataReader.GetInt32("Estado");
                            switch (novoSlot.Name)
                            {
                                case "LugarBaixo":
                                    if (novoSlot.estado == 0)
                                    {
                                        novoSlot.Image = Properties.Resources.lugarBaixo;
                                    }
                                    else
                                    {
                                        novoSlot.Image = Properties.Resources.lugarBaixoOcupado;
                                    }
                                    break;
                                case "LugarCima":
                                    if (novoSlot.estado == 0)
                                    {
                                        novoSlot.Image = Properties.Resources.lugarCima;
                                    }
                                    else
                                    {
                                        novoSlot.Image = Properties.Resources.lugarCimaOcupado;
                                    }
                                    break;
                                case "LugarEsquerda":
                                    if (novoSlot.estado == 0)
                                    {
                                        novoSlot.Image = Properties.Resources.lugarEsquerda;
                                    }
                                    else
                                    {
                                        novoSlot.Image = Properties.Resources.lugarEsquerdaOcupado;
                                    }
                                    break;
                                case "LugarDireita":
                                    if (novoSlot.estado == 0)
                                    {
                                        novoSlot.Image = Properties.Resources.lugarDireita;
                                    }
                                    else
                                    {
                                        novoSlot.Image = Properties.Resources.lugarDireitaOcupado;
                                    }
                                    break;
                                case "estrdaVerticalContinuo":
                                    novoSlot.Image = Properties.Resources.estradaVerticalContinuo;
                                    break;
                                case "estradaHorizontalContinuo":
                                    novoSlot.Image = Properties.Resources.estradaHorizontalContinuo;
                                    break;
                                case "estradaHorizontal":
                                    novoSlot.Image = Properties.Resources.estradaHorinzontal;
                                    break;
                                case "estradaVertical":
                                    novoSlot.Image = Properties.Resources.estradaVertical;
                                    break;
                                case "estradaBaixoDireita":
                                    novoSlot.Image = Properties.Resources.curvaBaixoDireita;
                                    break;
                                case "estradaCimaDireita":
                                    novoSlot.Image = Properties.Resources.curvaCimaDireita;
                                    break;
                                case "estradaBaixoEsquerda":
                                    novoSlot.Image = Properties.Resources.curvaBaixoEsquerda;
                                    break;
                                case "estradaCimaEsquerda":
                                    novoSlot.Image = Properties.Resources.curvaCimaEsquerda;
                                    break;
                                case "limpo":
                                    novoSlot.Image = Properties.Resources.limpo;
                                    break;
                                case "relva":
                                    novoSlot.Image = Properties.Resources.relva;
                                    break;
                                default: break;


                            }
                            novoSlot.BackColor = slot5.BackColor;
                            novoSlot.Width = slot5.Width;
                            novoSlot.Height = slot5.Height;
                            novoSlot.active = false;
                            novoSlot.MouseClick += ((o, a) =>
                            {
                                if (a.Button.ToString() == "Right")
                                {
                                    carroClicado = novoSlot;
                                    contextMenuStrip1.Show(Control.MousePosition);
                                }
                            });

                            dropArea.Controls.Add(novoSlot);
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro BD:\n" + ex.Message, "Login - getFieldsData()");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
                }
                else if (Global.check == true)
                {
                    MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                    try
                    {
                        MySqlCommand sqlCommand = new MySqlCommand("SELECT * FROM `LugarParque` WHERE `ParqueCodParque` = " + codigo, sqlConn);    //Comando SQL DML

                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD

                        while (sqlDataReader.Read())
                        {
                            Slot novoSlot = new Slot();
                            novoSlot.Name = sqlDataReader.GetString("Nome");
                            novoSlot.Location = new Point(sqlDataReader.GetInt32("X"), sqlDataReader.GetInt32("Y"));
                            novoSlot.estado = sqlDataReader.GetInt32("Estado");
                            novoSlot.Image = slot5.Image;
                            novoSlot.BackColor = slot5.BackColor;
                            novoSlot.Width = slot5.Width;
                            novoSlot.Height = slot5.Height;
                            novoSlot.active = false;
                            novoSlot.MouseClick += ((o, a) =>
                            {
                                if (a.Button.ToString() == "Right")
                                {
                                    carroClicado = novoSlot;
                                    contextMenuStrip1.Show(Control.MousePosition);
                                }
                            });

                            dropArea.Controls.Add(novoSlot);
                        }


                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro BD:\n" + ex.Message, "Login - getFieldsData()");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
                }
            }
        }

        /*private void slot1_Click(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro = new Slot();
            carro.Name = "LugarBaixo";
            carro.Location = dropArea.getNextAvailableLocation(slot1);
            //carro.active = false;
            carro.Image = slot1.Image;
            carro.BackColor = slot1.BackColor;
            carro.Width = slot1.Width;
            carro.Height = slot1.Height;

            //MessageBox.Show("Nome - " + carro.Name + "\nLocalizacao - " + carro.Location);

            //dropArea.Controls.Add(carro);
            carro.active = false;

            carro.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });


            //Reset slot 
            slot1.Location = slot1.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            //inserirCarro(carro.Location.X, carro.Location.Y, carro.Name);

            carroSelecionado = carro;

            codPlanta.Text = getLastTablePk();
        }

        private void slot2_Click(object sender, EventArgs e)
        {
            //Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro2 = new Slot();
            carro2.Name = "LugarCima";
            carro2.Location = dropArea.getNextAvailableLocation(slot2);
            //carro.active = false;
            carro2.Image = slot2.Image;
            carro2.BackColor = slot2.BackColor;
            carro2.Width = slot2.Width;
            carro2.Height = slot2.Height;
            //MessageBox.Show("Nome - " + carro2.Name + "\nLocalizacao - " + carro2.Location);

            //dropArea.Controls.Add(carro2);

            carro2.active = false;

            carro2.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro2;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });

            //Reset slot 
            slot2.Location = slot2.initLocation;
            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            //inserirCarro(carro2.Location.X, carro2.Location.Y, carro2.Name);
            carroSelecionado = carro2;

            codPlanta.Text = getLastTablePk();
        }*/

        private void dropArea_Paint(object sender, PaintEventArgs e)
        {
            
        }

        /*private void dropArea_Click(object sender, EventArgs e)
        {
            carroSelecionado.Location = new Point(X,Y);
            carroSelecionado.Location = dropArea.getNextAvailableLocation(carroSelecionado);
            dropArea.Controls.Add(carroSelecionado);
            inserirCarro(carroSelecionado.Location.X, carroSelecionado.Location.Y, carroSelecionado.Name);
            codPlanta.Text = getLastTablePk();
            
        }*/

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
            /*//Tell dropArea to Create a new slot on specific coordinates
            //dropArea.createSlot(slot1.Location);
            Slot carro = new Slot();
            carro.Name = "LugarBaixo";
            carro.Location = dropArea.getNextAvailableLocation(slot1);
            //carro.active = false;
            carro.Image = slot1.Image;
            carro.BackColor = slot1.BackColor;
            carro.Width = slot1.Width;
            carro.Height = slot1.Height;

            //MessageBox.Show("Nome - " + carro.Name + "\nLocalizacao - " + carro.Location);

            //dropArea.Controls.Add(carro);
            carro.active = false;

            carro.MouseClick += ((o, a) =>
            {
                if (a.Button.ToString() == "Right")
                {
                    carroClicado = carro;
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            });


            //Reset slot 
            slot1.Location = slot1.initLocation;

            dropArea.BackColor = Color.DimGray;
            dropArea.BackgroundImage = null;

            //inserirCarro(carro.Location.X, carro.Location.Y, carro.Name);

            carroSelecionado = carro;

            codPlanta.Text = getLastTablePk();*/
        }
    }


}
