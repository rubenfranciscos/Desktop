﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Planta
{
    class DroppableTarget : Panel
    {
        public int itemWidth = 20;
        public int itemHeigth = 20;

        public DroppableTarget()
        {

        }

        public Point getNextAvailableLocation(Slot slot)
        {
            //-95, -52
            int modulox =  (slot.Location.X - 95 +7) % itemHeigth;
            int moduloy = (slot.Location.Y - 52 +7) % itemWidth;
            Console.Write("m " + modulox + " " + moduloy);
            int newX = (slot.Location.X- 95 +7) - modulox;
            int newY = (slot.Location.Y - 52 +7) - moduloy;
            Console.WriteLine(slot.Name);
            Point newPoint = new Point(newX, newY);
            return newPoint;
        }
    }
}
