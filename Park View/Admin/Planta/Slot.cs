﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Park_View.Admin.Planta
{

   
    class Slot : DraggableObject
    {
        public event EventHandler onStopDrag;

        public String codigo;
        public Point initLocation;
        public int estado;

        public Slot()
        {
            codigo="";
            AllowDrop = true;
            
        }

        public override void startDrag()
        {
 	        base.startDrag();
            initLocation = this.Location;
        }

        public override void stopDrag()
        {
            base.stopDrag();
            if (onStopDrag != null)
            {
                // invoke the subscribed event-handler(s)
                onStopDrag(this, EventArgs.Empty);
            }
        }
    }
}
