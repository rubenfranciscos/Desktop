﻿using MySql.Data.MySqlClient;
using Park_View.Admin.Inicio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View.Admin.Parque
{
    public partial class Criar_Parque : Form
    {
        
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                          BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                          BDcodigo;//do alunos, caso se trate de um update ou insert

        private MySqlTransaction sqlTran = null;

        public Criar_Parque(String sgbd, String dml, String codigo)
        {
            InitializeComponent();
            BDsgbd = sgbd;
            BDdml = dml;
            BDcodigo = codigo;
        }

        private void btnCriarUtilizador_Click(object sender, EventArgs e)
        {

        }

        private void Criar_Parque_Load(object sender, EventArgs e)
        {
            this.Text = BDsgbd + " " + BDdml;



            switch (BDdml)
            {
                case "Inserir":
                    tbCod.Text = getLastTablePk();
                    tbMorada.Focus();
                    btnOK.BackgroundImage = Properties.Resources.add;
                    break;

                case "Update":
                    getFieldsData();
                    tbCod.Text = BDcodigo;
                    tbMorada.Focus();              //Focus nesta caixa
                    btnOK.BackgroundImage = Properties.Resources.edit;
                    break;

                case "Delete":
                    getFieldsData();

                    tbCod.Text = BDcodigo;
                    tbCod.Enabled = false;
                    tbMorada.Enabled = false;
                    tbNumeroLug.Enabled = false;
                    btnOK.Focus();

                    btnOK.BackgroundImage = Properties.Resources.delete;
                    break;

            }
        }

        private String getLastTablePk()
        {
            int pkCode = -1;                                                      // Recebe o codigo da tabela a usar nas SQL DML

            MessageBox.Show("" + Global.check.ToString());
            MessageBox.Show("PI: " + Global.checkPI.ToString());//Recebe a UtilsSQl uma ligação ao sgbd
            if (Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
                try
                {

                    if (Utils.getTest())
                    {
                        //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                    }
                    MySqlCommand sqlCommand = new MySqlCommand("Select MAX(CodParque) as CodParque from Parque", sqlConn);   //Comando SQL DML
                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                    if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                    {
                        while (sqlDataReader.Read())                             //Enquanto houver rows
                        {
                            String temp = sqlDataReader["CodParque"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                            if (Utils.getTest()) //MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCriar Parque");
                                if (temp == "")
                                {
                                    //MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (Utils.getTest()) //MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormParque");
                                        pkCode = int.Parse(temp) + 1;
                                }
                        }
                    }
                    else
                    {
                        //MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                        pkCode = 1;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormParque");

                }
                finally
                {
                    sqlConn.Close();
                }
            }
            else if (Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                try
                {

                    if (Utils.getTest())
                    {
                        //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");
                    }
                    MySqlCommand sqlCommand = new MySqlCommand("Select MAX(CodParque) as CodParque from Parque", sqlConn);   //Comando SQL DML
                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                    if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                    {
                        while (sqlDataReader.Read())                             //Enquanto houver rows
                        {
                            String temp = sqlDataReader["CodParque"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                            if (Utils.getTest()) //MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormCriar Parque");
                                if (temp == "")
                                {
                                    //MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                                    pkCode = 1;                                     //registo na textbox da codigo par a a

                                }
                                else
                                {
                                    if (Utils.getTest()) //MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormParque");
                                        pkCode = int.Parse(temp) + 1;
                                }
                        }
                    }
                    else
                    {
                        //MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                        pkCode = 1;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormParque");

                }
                finally
                {
                    sqlConn.Close();
                }
            }
            return pkCode.ToString();
        }


        private void getFieldsData()
        {
            if(Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
                if (Utils.getTest()) //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");
                    try
                    {
                        MySqlCommand sqlCommand = new MySqlCommand("Select Morada, Lugares from Parque where CodParque = @codigo", sqlConn);    //Comando SQL DML
                        sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (sqlDataReader.Read())                            //Enquanto houver rows
                            {
                                tbMorada.Text = sqlDataReader["Morada"].ToString();
                                tbNumeroLug.Text = sqlDataReader["Lugares"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Erro BD:\n" + e.Message, "FormCriarOrganizacao - getFieldsData()");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
            }
            else if(Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                if (Utils.getTest()) //MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");
                    try
                    {
                        MySqlCommand sqlCommand = new MySqlCommand("Select Morada, Lugares from Parque where CodParque = @codigo", sqlConn);    //Comando SQL DML
                        sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                        if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                        {
                            while (sqlDataReader.Read())                            //Enquanto houver rows
                            {
                                tbMorada.Text = sqlDataReader["Morada"].ToString();
                                tbNumeroLug.Text = sqlDataReader["Lugares"].ToString();
                            }
                        }
                        else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Erro BD:\n" + e.Message, "FormCriarOrganizacao - getFieldsData()");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
            }
            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(Global.checkPI == true)
            {
                MySqlConnection sqlConn = Utils.getSqlConnBDLocal();

                //validação de dados
                if (tbCod.Text == "" || tbMorada.Text == "" || tbNumeroLug.Text == "")
                {
                    MessageBox.Show("Não são permitidos campos vazios", "Atenção");
                }
                else
                {
                    try
                    {
                        switch (BDdml)
                        {
                            case "Inserir":

                                if (Utils.getTest())
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlInsert = new MySqlCommand("Insert into Parque (Morada, Lugares) VALUES(@Morada, @Lugares)", sqlConn);
                                sqlInsert.Parameters.AddWithValue("@CodParque", int.Parse(getLastTablePk()));
                                sqlInsert.Parameters.AddWithValue("@Morada", tbMorada.Text);
                                sqlInsert.Parameters.AddWithValue("@Lugares", tbNumeroLug.Text);

                                sqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                sqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL
                                sqlInsert.Transaction = sqlTran;                    //Ligação dos comandos à transação
                                sqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                sqlTran.Commit();                                   //Commit the transaction

                                break;

                            case "Update":
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlUpdate = new MySqlCommand("Update Parque SET Morada = @Morada, Lugares = @Lugares WHERE CodParque = @CodParque", sqlConn);
                                sqlUpdate.Parameters.AddWithValue("@CodParque", int.Parse(BDcodigo));
                                sqlUpdate.Parameters.AddWithValue("@Morada", tbMorada.Text);
                                sqlUpdate.Parameters.AddWithValue("@Lugares", tbNumeroLug.Text);

                                sqlConn.Open();                                                 //Abertura a base de dados
                                sqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                break;

                            case "Delete":
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlDelete = new MySqlCommand("Delete from Parque where CodParque = @CodParque", sqlConn);
                                sqlDelete.Parameters.AddWithValue("@CodParque", int.Parse(BDcodigo));

                                sqlConn.Open();                                                 //Abertura a base de dados
                                sqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                break;

                            default:
                                //MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: FormAluno - Botão OK - switch default");
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("Erro BD:\n" + ex.Message, "ERRO: FormAluno - Botão OK - switch()");
                        try
                        {
                            sqlTran.Rollback();
                        }
                        catch (Exception exRollback)
                        {
                            //MessageBox.Show("Erro BD:\n" + exRollback.Message, "ERRO: FormAluno - Botão OK - switch rollback");
                        }
                    }
                    finally
                    {
                        sqlConn.Close();

                        MessageBox.Show("Procedimento concuido", "INFO");

                        this.Close();
                    }

                }
            }
            else if(Global.check == true)
            {
                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();

                //validação de dados
                if (tbCod.Text == "" || tbMorada.Text == "" || tbNumeroLug.Text == "")
                {
                    MessageBox.Show("Não são permitidos campos vazios", "Atenção");
                }
                else
                {
                    try
                    {
                        switch (BDdml)
                        {
                            case "Inserir":
                     
                                if (Utils.getTest()) 
                                {
    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlInsert = new MySqlCommand("Insert into Parque (Morada, Lugares) VALUES(@Morada, @Lugares)", sqlConn);
                                sqlInsert.Parameters.AddWithValue("@CodParque", int.Parse(getLastTablePk()));
                                sqlInsert.Parameters.AddWithValue("@Morada", tbMorada.Text);
                                sqlInsert.Parameters.AddWithValue("@Lugares", tbNumeroLug.Text);

                                sqlConn.Open();                                     //abertura da ligaçã ao sgbd
                                sqlTran = sqlConn.BeginTransaction();               //transação para controlo da operação SQL
                                sqlInsert.Transaction = sqlTran;                    //Ligação dos comandos à transação
                                sqlInsert.ExecuteNonQuery();                        //Executa o SQL DML
                                sqlTran.Commit();                                   //Commit the transaction

                                break;

                            case "Update":
                                if (Utils.getTest()) 
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlUpdate = new MySqlCommand("Update Parque SET Morada = @Morada, Lugares = @Lugares WHERE CodParque = @CodParque", sqlConn);
                                sqlUpdate.Parameters.AddWithValue("@CodParque", int.Parse(BDcodigo));
                                sqlUpdate.Parameters.AddWithValue("@Morada", tbMorada.Text);
                                sqlUpdate.Parameters.AddWithValue("@Lugares", tbNumeroLug.Text);

                                sqlConn.Open();                                                 //Abertura a base de dados
                                sqlUpdate.ExecuteNonQuery();                                    //Executa o SQL DML
                                break;

                            case "Delete":
                                if (Utils.getTest())
                                {
                                    //MessageBox.Show("Vou fazer o DML" + BDdml, "TESTES");
                                }
                                MySqlCommand sqlDelete = new MySqlCommand("Delete from Parque where CodParque = @CodParque", sqlConn);
                                sqlDelete.Parameters.AddWithValue("@CodParque", int.Parse(BDcodigo));

                                sqlConn.Open();                                                 //Abertura a base de dados
                                sqlDelete.ExecuteNonQuery();                                    //Executa o SQL DML
                                break;

                            default:
                                //MessageBox.Show("DML Inderterminado: " + BDdml, "ERRO: FormAluno - Botão OK - switch default");
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("Erro BD:\n" + ex.Message, "ERRO: FormAluno - Botão OK - switch()");
                        try
                        {
                            sqlTran.Rollback();
                        }
                        catch (Exception exRollback)
                        {
                            //MessageBox.Show("Erro BD:\n" + exRollback.Message, "ERRO: FormAluno - Botão OK - switch rollback");
                        }
                    }
                    finally
                    {
                        sqlConn.Close();

                        MessageBox.Show("Procedimento concuido", "INFO");
                    
                        this.Close();
                    }
                }
            }            
        }

        private void btnCncael_Click(object sender, EventArgs e)
        {
            this.Hide();
        }



    }
}
