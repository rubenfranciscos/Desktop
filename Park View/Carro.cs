﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Park_View
{
    class Carro
    {

        private string origem;
        private string destino;


        private Point coordenadasGpsOrigem = new Point();
        private Point coordenadasGpsDestino = new Point();
        private Point coordenadasGpsActual = new Point();


        public void setLugarOrigem(Point p)           //Set Origem
        {
            coordenadasGpsOrigem = p;
        }

        public void setLugarDestino(Point p)        //Set Destino
        {
            coordenadasGpsDestino = p;
        }


        public void setCoordernadaActual(Point p)   //Set Coordenada Actual
        {
            coordenadasGpsActual = p;
        }

        public Point getCoordenadaOrigem()       //Get Coordenadas Origem
        {
            return coordenadasGpsOrigem;
        }

        public Point getCoordenadaDestino()     //Get Coordenadas  Destino
        {
            return coordenadasGpsDestino;
        }

        public Point getCoordenadaActual()      //Get Coordenadas Actuais
        {
            return coordenadasGpsActual;
        }

    }
}
