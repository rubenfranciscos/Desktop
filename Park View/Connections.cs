﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Park_View
{
    static class Connections
    {
        static MySqlConnection mySqlConnection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=");
        static MySqlCommand mySqlCommand;

        public static void openConnection()
        {
            if (mySqlConnection.State == System.Data.ConnectionState.Closed)
            {
                mySqlConnection.Open();
               // MessageBox.Show("Base de dados Aberta");
            }
        }

        public static void closeConnection()
        {
            if(mySqlConnection.State == System.Data.ConnectionState.Open)
            {
                mySqlConnection.Close();
                //MessageBox.Show("Base de dados Fechada");
            }
            
        }

        public static void executeQuery(string q)
        {
            try
            {
                openConnection();
                mySqlCommand = new MySqlCommand(q, mySqlConnection);
                if(mySqlCommand.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Comando executado com sucesso");
                }
                else
                {
                    //MessageBox.Show("Erro ao executar o comando SQL");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        }
    }
}
