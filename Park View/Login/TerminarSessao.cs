﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View
{
    public partial class TerminarSessao : Form
    {
        public TerminarSessao()
        {
            InitializeComponent();
        }

        private void TerminarSessao_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            btnLogin login = new btnLogin();
            login.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();

            Entrada entrada = new Entrada();
            entrada.ShowDialog();
        }
    }
}
