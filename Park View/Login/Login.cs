﻿using MySql.Data.MySqlClient;
using Park_View.Admin.Inicio;
using Park_View.Funcionario.Inicio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Park_View
{
    
    public partial class btnLogin : Form
    {
        //static Boolean cb;
        public btnLogin()
        {
            InitializeComponent();
            this.AcceptButton = button1;
        }

        private void btnLogin_Load(object sender, EventArgs e)
        {
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Entrada cancel = new Entrada();
            cancel.ShowDialog();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            //Entrada.Hide();
            //this.Hide();
            PrincipalAdmin pincipal = new PrincipalAdmin();
            //pincipal.ShowDialog();
            
            bool pass = false;

            string tipoEntrada = "";

            if(rbON.Checked == true){

                MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                try
                {
                    MySqlCommand sqlCommand = new MySqlCommand("Select CodUtilizador, Username, Password, TipoUtilizador from Utilizador where  Username = @Username AND Password = @Password", sqlConn);    //Comando SQL DML
                    sqlCommand.Parameters.AddWithValue("@Username", tbName.Text);
                    sqlCommand.Parameters.AddWithValue("@Password", tbPass.Text);
                    sqlCommand.Parameters.AddWithValue("@TipoUtilizador", tipoEntrada);

                    sqlCommand.CommandType = CommandType.Text;

                    sqlConn.Open();                                             //abertura a base de dados
                    MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD

                    /*
                    while (sqlDataReader.Read())                            //Enquanto houver rows
                    {
                        tbName.Text = sqlDataReader["Username"].ToString();
                        tbPass.Text = sqlDataReader["Password"].ToString();
                        tipoEntrada = sqlDataReader["TipoUtilizador"].ToString();
                        if(tbName.Text == sqlDataReader["Username"].ToString() && tbPass.Text == sqlDataReader["Password"].ToString())
                        {
                            //MessageBox.Show(pass.ToString());
                            pass = true;
                        }/*
                        else
                        {
                            MessageBox.Show("UserName Ou Passwor errados");
                        }*/
                    /*}*/
                    if (sqlDataReader.Read())
                    {
                        if (sqlDataReader.GetString("TipoUtilizador").Equals("Admin"))
                        {
                            MessageBox.Show("Admin!!!");
                        }
                        else
                        {
                            MessageBox.Show("Funcionaria");
                        }
                        Global.codigoUtilizador = sqlDataReader.GetString(0);
                        Global.utilizador = tbName.Text;
                        MessageBox.Show("Entrou!" + Global.codigoUtilizador);
                        this.Hide();
                        pincipal.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Acesso negado!");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro BD:\n" + ex.Message, "Login - getFieldsData()");
                }
                finally
                {
                    sqlConn.Close();
                }

            }
            else
            {
                if(rbPI.Checked == true)
                {
                    MySqlConnection sqlConn = Utils.getSqlConnBDLocal();
                    try
                    {
                        MySqlCommand sqlCommand = new MySqlCommand("Select CodUtilizador, Username, Password, TipoUtilizador from Utilizador where  Username = @Username AND Password = @Password", sqlConn);    //Comando SQL DML
                        sqlCommand.Parameters.AddWithValue("@Username", tbName.Text);
                        sqlCommand.Parameters.AddWithValue("@Password", tbPass.Text);
                        sqlCommand.Parameters.AddWithValue("@TipoUtilizador", tipoEntrada);

                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD

                        /*
                        while (sqlDataReader.Read())                            //Enquanto houver rows
                        {
                            tbName.Text = sqlDataReader["Username"].ToString();
                            tbPass.Text = sqlDataReader["Password"].ToString();
                            tipoEntrada = sqlDataReader["TipoUtilizador"].ToString();
                            if(tbName.Text == sqlDataReader["Username"].ToString() && tbPass.Text == sqlDataReader["Password"].ToString())
                            {
                                //MessageBox.Show(pass.ToString());
                                pass = true;
                            }/*
                            else
                            {
                                MessageBox.Show("UserName Ou Passwor errados");
                            }*/
                        /*}*/
                        if (sqlDataReader.Read())
                        {
                            if (sqlDataReader.GetString("TipoUtilizador").Equals("Admin"))
                            {
                                MessageBox.Show("Admin!!!");
                            }
                            else
                            {
                                MessageBox.Show("Funcionaria");
                            }
                            Global.codigoUtilizador = sqlDataReader.GetString(0);
                            Global.utilizador = tbName.Text;
                            MessageBox.Show("Entrou!" + Global.codigoUtilizador);
                            this.Hide();
                            pincipal.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show("Acesso negado!");
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro BD:\n" + ex.Message, "Login - getFieldsData()");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
                }
                else if (rbON.Checked == true)
                {
                    MySqlConnection sqlConn = Utils.getMySqlConnBDLocalOnline();
                    try
                    {
                        MySqlCommand sqlCommand = new MySqlCommand("Select CodUtilizador, Username, Password, TipoUtilizador from Utilizador where  Username = @Username AND Password = @Password", sqlConn);    //Comando SQL DML
                        sqlCommand.Parameters.AddWithValue("@Username", tbName.Text);
                        sqlCommand.Parameters.AddWithValue("@Password", tbPass.Text);
                        sqlCommand.Parameters.AddWithValue("@TipoUtilizador", tipoEntrada);

                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();                                             //abertura a base de dados
                        MySqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD

                        /*
                        while (sqlDataReader.Read())                            //Enquanto houver rows
                        {
                            tbName.Text = sqlDataReader["Username"].ToString();
                            tbPass.Text = sqlDataReader["Password"].ToString();
                            tipoEntrada = sqlDataReader["TipoUtilizador"].ToString();
                            if(tbName.Text == sqlDataReader["Username"].ToString() && tbPass.Text == sqlDataReader["Password"].ToString())
                            {
                                //MessageBox.Show(pass.ToString());
                                pass = true;
                            }/*
                            else
                            {
                                MessageBox.Show("UserName Ou Passwor errados");
                            }*/
                        /*}*/
                        if (sqlDataReader.Read())
                        {
                            if(sqlDataReader.GetString("TipoUtilizador").Equals("Admin"))
                            {
                                MessageBox.Show("Admin!!!");
                            }
                            else
                            {
                                MessageBox.Show("Funcionaria");
                            }
                            Global.codigoUtilizador = sqlDataReader.GetString(0);
                            Global.utilizador = tbName.Text;
                            MessageBox.Show("Entrou!" + Global.codigoUtilizador);
                            this.Hide();
                            pincipal.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show("Acesso negado!");
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro BD:\n" + ex.Message, "Login - getFieldsData()");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
                }
            }

            /*if(pass)
            {
                MessageBox.Show(pass.ToString());
                if (tipoEntrada == "Admin")
                {
                    this.Hide();
                    PrincipalAdmin pisncipal = new PrincipalAdmin();
                    pincipal.ShowDialog();
                    //pincipal.Show();
                }
                else if (tipoEntrada == "Funcionario")
                {
                    this.Hide();
                    Principal_Funcionario pincipalFunc = new Principal_Funcionario();
                    pincipalFunc.ShowDialog();
                    //pincipalFunc.Show();
                }

                
            }
            else
            {
                //MessageBox.Show("UserName Ou Passwor errados");
            }*/
        }

        private void panelClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            ActiveForm.WindowState = FormWindowState.Minimized;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tbName.Text = "severino100";
            tbPass.Text = "123";
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbON_CheckedChanged(object sender, EventArgs e)
        {

            Global.check = true;
            Global.checkPI = false;
        }

        private void rbPI_CheckedChanged(object sender, EventArgs e)
        {
            Global.check = false;
            Global.checkPI = true;
        }
    }
}
